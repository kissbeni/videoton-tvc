
; init stack pointer
ld sp, $07ff

call run_memtest

ld sp, $f000

call initialize_terminal
call beep

ld hl, s_welcome_msg
call putstr
ld a, $00
ld (column_index), a
ld a, $02
ld (row_index), a
call calculate_cursor_pointer

main_loop:
    call update_status_bar
    call show_cursor
    call receive_char
    ld b, a
    call remove_cursor
    ld a, b
    sub $20
    ld a, b
    jr nc, main_loop_not_ctrl
    call custom_handler_switch
    jr z, main_loop
    main_loop_not_ctrl:
    call putchar
    jp main_loop

include "special_handlers.asm"
include "escape_seq.asm"
include "screen.asm"
include "cursor.asm"
include "status_bar.asm"
include "termio.asm"
include "utils.asm"
include "memtest.asm"

; Constants:
color_code_lut:             DEFB $00, $01, $06, $07, $08, $09, $0e, $0f
s_welcome_msg:              DEFB "Terminal ready.", $00
s_memtest_error:            DEFB "Memory test failed. System halted.", $00

; RAM:
character_pointer:          DW $4000
column_index:               DB $00
row_index:                  DB $00
current_color:              DB $00
cursor_color_backup:        DB $00
escape_sequence_buffer_len: DB $00
escape_sequence_buffer:     DEFB $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
