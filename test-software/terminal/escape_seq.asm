
escape_handler_switch:
    cp $66 ; f
    call z, handle_escape_move_cursor_to
    ret z
    cp $44 ; D
    call z, handle_escape_cursor_left
    ret z
    cp $43 ; C
    call z, handle_escape_cursor_right
    ret z
    cp $41 ; A
    call z, handle_escape_cursor_up
    ret z
    cp $42 ; B
    call z, handle_escape_cursor_down
    ret z
    cp $48 ; H
    call z, handle_escape_cursor_home
    ret z
    cp $56 ; V
    call z, handle_escape_rle_video
    ret z
    cp $6d ; m
    call z, handle_escape_format
    ret

handle_escape_cursor_left:
    push af
    call get_escape_command_repeat_count
    handle_escape_cursor_left_loop:
        call move_cursor_left
        dec b
        jr nz, handle_escape_cursor_left_loop
    pop af
    ret

handle_escape_cursor_right:
    push af
    call get_escape_command_repeat_count
    handle_escape_cursor_right_loop:
        call move_cursor_right
        dec b
        jr nz, handle_escape_cursor_right_loop
    pop af
    ret

handle_escape_cursor_up:
    push af
    call get_escape_command_repeat_count
    handle_escape_cursor_up_loop:
        call move_cursor_up
        dec b
        jr nz, handle_escape_cursor_up_loop
    pop af
    ret

handle_escape_cursor_down:
    push af
    call get_escape_command_repeat_count
    handle_escape_cursor_down_loop:
        call move_cursor_down
        dec b
        jr nz, handle_escape_cursor_down_loop
    pop af
    ret

; gives the repeat count in b
get_escape_command_repeat_count:
    ld hl, escape_sequence_buffer
    call parse_decimal_from_ascii
    ld b, a
    or a
    ret nz
    ld b, $01
    ret

handle_escape_cursor_home:
    push af
    ld hl, $4000
    ld (character_pointer), hl
    ld a, $00
    ld (column_index), a
    ld (row_index), a
    pop af
    ret

handle_escape_format:
    push af
    ld hl, escape_sequence_buffer
    ld a, (escape_sequence_buffer_len)
    cp $02
    jr nz, handle_escape_format_not_2

    ld a, (hl)
    inc hl
    cp $33
    jr z, handle_escape_format_fg
    cp $34
    jr z, handle_escape_format_bg
    jr handle_escape_format_end

    handle_escape_format_fg:
    ld a, (hl)
    cp $39
    jr z, handle_escape_format_fg_default
    call map_color
    rla
    rla
    rla
    rla
    and $f0
    ld b, a
    ld a, (current_color)
    and $0f
    or b
    jr handle_escape_format_set

    handle_escape_format_bg:
    ld a, (hl)
    cp $39
    jr z, handle_escape_format_bg_default
    call map_color
    ld b, a
    ld a, (current_color)
    and $f0
    or b
    jr handle_escape_format_set

    handle_escape_format_fg_default:
    ld a, (current_color)
    or $f0
    jr handle_escape_format_set

    handle_escape_format_bg_default:
    ld a, (current_color)
    and $f0
    jr handle_escape_format_set

    handle_escape_format_not_2:
    cp $01
    jr nz, handle_escape_format_end

    ld a, (hl)
    cp $30
    jr nz, handle_escape_format_end

    ; [0m -> reset color
    ld a, $f0

    handle_escape_format_set:
    ld (current_color), a
    ld (cursor_color_backup), a

    handle_escape_format_end:
    pop af
    ret

map_color:
    push hl
    push de
    and $07
    ld hl, color_code_lut
    ld d, $00
    ld e, a
    add hl, de
    ld a, (hl)
    pop de
    pop hl
    ret

handle_escape_move_cursor_to:
    push af
    ld hl, escape_sequence_buffer
    call parse_decimal_from_ascii
    ld b, a
    ld a, (hl)
    inc hl
    cp $3b ; ';'
    jr nz, handle_escape_move_cursor_to_end

    call parse_decimal_from_ascii
    ld c, a

    call handle_escape_cursor_home

    ld a, b
    ld (row_index), a
    ld a, c
    ld (column_index), a
    call calculate_cursor_pointer

    handle_escape_move_cursor_to_end:
    pop af
    ret

handle_escape_rle_video:
    push af
    call clear_screen
    call clear_screen_color

    ld hl, $4000

    handle_escape_rle_video_loop:
        ld d, $00
        handle_escape_rle_video_receive_char:
        in a, ($f3)
        bit 1, a
        jr z, handle_escape_rle_video_receive_char
        in a, ($f2)
        cp $ff
        jr z, handle_escape_rle_video_end
        bit 7, a
        jr z, handle_escape_rle_video_no_set
        ld d, $ff
        handle_escape_rle_video_no_set:
        and $7f
        jr z, handle_escape_rle_video_reset_ptr

        handle_escape_rle_video_rle_loop:
        ld (hl), d
        inc hl
        dec a
        jr nz, handle_escape_rle_video_rle_loop
        jr handle_escape_rle_video_loop

        handle_escape_rle_video_reset_ptr:
        ld hl, $4000
        jr handle_escape_rle_video_loop

    handle_escape_rle_video_end:
    call clear_screen_color
    call init_status_bar
    pop af
    ret

; parses a decimal ascii integer at memory location in hl
parse_decimal_from_ascii:
    push bc
    ld c, $00

    parse_decimal_from_ascii_loop:
    ld a, (hl)
    ld b, a

    and $f0
    cp $30
    jr nz, parse_decimal_from_ascii_end
    ld a, b
    cp $3b
    jr z, parse_decimal_from_ascii_end

    ld a, c
    call mul10
    ld c, a
    ld a, b
    and $0f
    add c
    ld c, a

    inc hl
    jr parse_decimal_from_ascii_loop

    parse_decimal_from_ascii_end:
    ld a, c
    pop bc
    ret

mul10:
    push bc
    ld b, a
    sla a
    ld c, a
    ld a, b
    sla a
    sla a
    sla a
    add c
    pop bc
    ret
