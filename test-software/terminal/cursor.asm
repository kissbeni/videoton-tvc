show_cursor:
    push hl

    ; Store cell color at cursor
    ld hl, (character_pointer)
    ld a, h
    or $20
    ld h, a
    ld a, (hl)

    ld (cursor_color_backup), a
    ld a, $27
    ld (hl), a
    pop hl
    ret

remove_cursor:
    push hl

    ; Restore cell color at cursor
    ld hl, (character_pointer)
    ld a, h
    or $20
    ld h, a
    ld a, (cursor_color_backup)
    ld (hl), a

    pop hl
    ret

move_cursor_left:
    push hl
    ld hl, (character_pointer)
    ld a, h
    cp $40
    jr nz, move_cursor_left_limit_ok
    ld a, l
    or a
    jr nz, move_cursor_left_limit_ok
    jr move_cursor_left_end
    move_cursor_left_limit_ok:
    dec hl
    ld (character_pointer), hl

    ld hl, column_index
    ld a, (hl)
    or a
    jr nz, move_cursor_column_dec

    ld a, $4f
    ld (hl), a

    ld hl, row_index
    dec (hl)
    jr move_cursor_left_end

    move_cursor_column_dec:
    ld hl, column_index
    dec (hl)

    move_cursor_left_end:
    pop hl
    ret

move_cursor_right:
    push hl
    ld hl, (character_pointer)
    ld a, h
    cp $52
    jr nz, move_cursor_right_limit_ok
    ld a, l
    cp $6f
    jr nz, move_cursor_right_limit_ok
    jr move_cursor_right_end
    move_cursor_right_limit_ok:
    inc hl
    ld (character_pointer), hl

    ld hl, column_index
    inc (hl)
    ld a, (hl)
    cp $50
    jr nz, move_cursor_right_end

    ld a, $00
    ld (hl), a

    ld hl, row_index
    inc (hl)

    move_cursor_right_end:
    pop hl
    ret

move_cursor_up:
    ld a, (row_index)
    cp $00
    ret z
    push hl
    ld hl, row_index
    dec (hl)
    call calculate_cursor_pointer
    pop hl
    ret

move_cursor_down:
    ld a, (row_index)
    cp $3a
    ret z
    push hl
    ld hl, row_index
    inc (hl)
    call calculate_cursor_pointer
    pop hl
    ret

calculate_cursor_pointer:
    push hl
    push de

    ld hl, $4000

    ld a, (row_index)
    or a
    jr z, calculate_cursor_pointer_row_loop_skip

    ld de, $0050
    calculate_cursor_pointer_row_loop:
        add hl, de
        dec a
        jr nz, calculate_cursor_pointer_row_loop

    calculate_cursor_pointer_row_loop_skip:

    ld de, $0000
    ld a, (column_index)
    ld e, a
    add hl, de

    ld (character_pointer), hl

    pop de
    pop hl
    ret
