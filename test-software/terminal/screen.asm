increment_column_idx:
    ld hl, column_index
    inc (hl)
    ld a, (hl)
    cp $50
    ret nz
    call increment_row_idx

    ld a, $00
    ld (column_index), a
    ret

increment_row_idx:
    ld hl, row_index
    inc (hl)
    ld a, (hl)
    cp $3b
    ret nz
    dec (hl)
    jp scroll_screen

clear_screen_color:
    push hl
    ld hl, $6000
    clear_screen_color_loop:
    ld (hl), $f0
    inc hl
    ld a, h
    cp $72
    jr nz, clear_screen_color_loop
    ld a, l
    cp $c0
    jr nz, clear_screen_color_loop

    ld a, $f0
    ld (current_color), a
    pop hl
    ret

clear_screen:
    push hl
    ld hl, $4000
    ld (character_pointer), hl
    clear_screen_loop:
    ld (hl), $00
    inc hl
    ld a, h
    cp $52
    jr nz, clear_screen_loop
    ld a, l
    cp $c0
    jr nz, clear_screen_loop

    ld a, $00
    ld (column_index), a
    ld (row_index), a

    pop hl
    ret

scroll_screen:
    push hl
    push de
    push bc

    ld hl, $4050
    ld de, $4000
    scroll_screen_loop:
    ld a, (hl)
    ld (de), a
    ld b, h
    ld c, d
    ld a, h
    or $20
    ld h, a
    ld a, d
    or $20
    ld d, a
    ld a, (hl)
    ld (de), a
    ld h, b
    ld d, c
    inc de
    inc hl
    ld a, h
    cp $52
    jr nz, scroll_screen_loop
    ld a, l
    cp $70
    jr nz, scroll_screen_loop

    ex de, hl
    scroll_screen_loop_2:
    ld a, $00
    ld (hl), a
    ld b, h
    ld a, h
    or $20
    ld h, a
    ld a, $f0
    ld (hl), a
    ld h, b
    inc hl
    ld a, h
    cp $52
    jr nz, scroll_screen_loop_2
    ld a, l
    cp $70
    jr nz, scroll_screen_loop_2

    ld a, $00
    ld (column_index), a

    call calculate_cursor_pointer

    pop bc
    pop de
    pop hl
    ret
