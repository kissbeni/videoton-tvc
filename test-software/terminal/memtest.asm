
run_memtest:
    ld hl, $1000
    memtest_fill_loop:
    ld a, l
    ld (hl), a
    inc hl
    ld a, h
    cp $df
    jr nz, memtest_fill_loop

    ld hl, $1000
    memtest_check_loop:
    ld a, (hl)
    cp l
    jr nz, memtest_fail
    inc hl
    ld a, h
    cp $df
    jr nz, memtest_check_loop

    ld hl, $1000
    memtest_fill_loop_inv:
    ld a, l
    xor $ff
    ld (hl), a
    inc hl
    ld a, h
    cp $df
    jr nz, memtest_fill_loop_inv

    ld hl, $1000
    memtest_check_loop_inv:
    ld a, (hl)
    xor $ff
    cp l
    jr nz, memtest_fail
    inc hl
    ld a, h
    cp $df
    jr nz, memtest_check_loop_inv

    ret

    memtest_fail:
    call initialize_terminal
    call dump_mem_state
    call beep
    call wait2
    call beep
    call wait2
    call beep
    ld hl, s_memtest_error
    call putstr
    halt

dump_mem_state:
    ex de, hl
    ld hl, $4050
    ld a, d
    call print_byte
    ld a, e
    call print_byte

    ret

    ld hl, $40a0
    ld c, $10

    mem_dump_loop:
    inc hl
    ld a, (de)
    call print_byte

    dec c
    inc de
    or c
    jr nz, mem_dump_loop

    ret
