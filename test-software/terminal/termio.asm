putchar:
    push hl
    ld hl, (character_pointer)
    inc hl
    ld (character_pointer), hl
    dec hl
    ld (hl), a

    ld a, h
    or $20
    ld h, a
    ld a, (current_color)
    ld (hl), a

    call increment_column_idx
    pop hl
    ret

putstr:
    push hl
    putstr_loop:
    ld a, (hl)
    or a
    jr z, putstr_loop_end
    inc hl
    call putchar
    jr putstr_loop
    putstr_loop_end:
    pop hl
    ret

print_byte:
    push bc
    ld c, a
    call print_byte_a
    ld (hl), a
    inc hl
    ld a, c
    call print_byte_b
    ld (hl), a
    inc hl
    pop bc
    ret

print_byte_a:
    rra
    rra
    rra
    rra
print_byte_b:
    or $F0
    daa
    add a, $A0
    adc a, $40
    ret
