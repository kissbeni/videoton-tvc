update_status_bar:
    push hl

    ld a, (row_index)
    ld hl, $5272
    call print_byte

    ld a, (column_index)
    ld hl, $5275
    call print_byte

    ld hl, (character_pointer)
    ld a, h
    ld b, l

    ld hl, $5278
    call print_byte
    ld a, b
    call print_byte

    ld hl, $5280
    ld a, (escape_sequence_buffer_len)
    call print_byte
    inc hl
    ld de, escape_sequence_buffer
    ld b, $08
    update_status_bar_cmd_dump_loop:
    ld a, (de)
    call print_byte
    inc de
    dec b
    jr nz, update_status_bar_cmd_dump_loop

    pop hl
    ret

init_status_bar:
    ld hl, $7270
    init_status_bar_loop:
    ld (hl), $f1
    inc hl
    ld a, h
    cp $72
    jr nz, init_status_bar_loop
    ld a, l
    cp $c0
    jr nz, init_status_bar_loop

    ld hl, $5274
    ld a, $3a
    ld (hl), a
    ret
