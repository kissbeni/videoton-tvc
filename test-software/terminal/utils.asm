
initialize_terminal:
    push hl
    call    clear_screen
    call    clear_screen_color
    call    init_status_bar
    pop hl
    ret

receive_char:
	in	a, ($f3)
	bit	1, a
	jr	z, receive_char
    in  a, ($f2)
    ret

wait2:
    push de
    ld de, $2500
    wait_inner2:
    dec de
    ld a, d
    or e
    jr nz, wait_inner2
    pop de
    ret

beep:
    ld a, $ff
    out ($06), a
    ld a, $3c
    out ($04), a
    ld a, $ff
    out ($05), a

    call wait2

    ld a, $00
    out ($05), a
    ret
