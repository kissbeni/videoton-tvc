
custom_handler_switch:
    cp $1b
    call z, handle_escape
    ret z
    cp $0a
    call z, handle_linefeed
    ret z
    cp $0d
    call z, handle_carriage_return
    ret z
    cp $08
    call z, handle_backspace
    ret z
    cp $07
    call z, handle_bell
    ret z
    cp $18
    call z, handle_clear_screen
    ret

handle_linefeed:
    push af
    ld a, (row_index)
    cp $3a
    jr nz, handle_linefeed_no_scroll
    call scroll_screen
    jr handle_linefeed_end
    handle_linefeed_no_scroll:
    call move_cursor_down
    handle_linefeed_end:
    pop af
    ret

handle_carriage_return:
    push af
    push bc

    handle_carriage_return_loop:
    ld hl, column_index
    ld a, (hl)
    or a
    jr z, handle_carriage_return_loop_end
    call move_cursor_left
    jr handle_carriage_return_loop

    handle_carriage_return_loop_end:
    pop bc
    pop af
    ret

handle_backspace:
    push af
    call move_cursor_left
    ld a, $00
    ld hl, (character_pointer)
    ld (hl), a
    ld a, h
    or $20
    ld h, a
    ld (hl), $f0
    pop af
    ret

handle_bell:
    push af
    call beep
    pop af
    ret

handle_clear_screen:
    push af
    call clear_screen
    call clear_screen_color
    call init_status_bar
    pop af
    ret

handle_escape:
    push af

    call receive_char
    cp $5b ; [
    jp nz, handle_escape_finish

    ld b, $08
    ld hl, escape_sequence_buffer
    ld de, escape_sequence_buffer_len
    ld a, $00
    ld (de), a

    handle_escape_read_loop:
    call receive_char
    ld (hl), a
    inc hl

    cp $3b ; ';'
    jr z, handle_escape_read_loop_continue

    ld c, a
    and $f0
    cp $30
    jr nz, handle_escape_read_loop_done
    ld a, c
    and $08
    jr z, handle_escape_read_loop_continue
    ld a, c
    and $06
    jr nz, handle_escape_read_loop_done

    handle_escape_read_loop_continue:
    ex de, hl
    inc (hl)
    ex de, hl
    dec b
    jr nz, handle_escape_read_loop

    handle_escape_read_loop_done:
    dec hl
    ld a, (hl)

    call escape_handler_switch

    handle_escape_finish:
    pop af
    ret
