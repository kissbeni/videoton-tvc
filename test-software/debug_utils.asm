
transmit_char:
    push af
    out ($f2), a
    uart_wait:
    in a, ($F3)
    bit 0, a
    jr nz, uart_wait
    pop af
    ret

receive_char:
	in	a, ($f3)
	bit	1, a
	jr	z, receive_char
    in a, ($f2)
    call transmit_char
    ret

wait:
    push de
    ld de, 0xFF00
    wait_inner1:
    dec de
    ld a, d
    or e
    jr NZ, wait_inner1
    pop de
    ret

wait2:
    push de
    ld de, 0x2500
    wait_inner2:
    dec de
    ld a, d
    or e
    jr NZ, wait_inner2
    pop de
    ret

print_linebreak:
    push bc
    ld b, a
    ld a, 13
    call transmit_char
    ld a, 10
    call transmit_char
    ld a, b
    pop bc
    ret

print_byte:
    push bc
    ld c, a
    call print_byte_a
    call transmit_char
    ld a, c
    call print_byte_b
    call transmit_char
    pop bc
    ret

print_byte_a:
    rra
    rra
    rra
    rra
print_byte_b:
    or $F0
    daa
    add a, $A0
    adc a, $40
    ret

prints:
    ld a, (hl)
    or a
    ret z
    call transmit_char
    inc hl
    jr prints
    ret

prints_screen:
    ld de, $4000
    prints_screen_loop:
    ld a, (hl)
    or a
    ret z
    ex de, hl
    ld (hl), a
    ex de, hl
    inc de
    inc hl
    jr  prints_screen_loop
    ret

print_hexdump_line:
    push hl
    push bc
    ld a, h
    call print_byte
    ld a, l
    call print_byte
    ld a, $3a ; :
    call transmit_char

    ld c, $10

    hexdump_loop:
    ld a, $20 ; space
    call transmit_char
    ld a, (hl)
    call print_byte

    dec c
    inc hl
    or c
    jr nz, hexdump_loop

    call print_linebreak
    pop bc
    pop hl
    ret
