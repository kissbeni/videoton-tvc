
; init stack pointer
ld      ix, $07ff
ld      sp, ix

ld a, $ff
out ($06), a

main_loop:
    call receive_char
    out ($04), a

    call receive_char
    out ($05), a

    jr main_loop

receive_char:
	in	a, ($f3)
	bit	1, a
	jr	z, receive_char
    in  a, ($f2)
    ret
