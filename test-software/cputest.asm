
beg:

ld A, 0xDE
out (0xF0), A

ld A, 0xAD
out (0xF1), A

ld DE, 0xFF00
wait_inner1:
dec DE
ld A, D
or E
jp NZ, wait_inner1

ld A, 0xBE
out (0xF0), A

ld A, 0xEF
out (0xF1), A

ld DE, 0xFF00
wait_inner2:
dec DE
ld A, D
or E
jp NZ, wait_inner2

jp beg
