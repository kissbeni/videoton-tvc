
; init stack pointer
ld ix, $03ff
ld sp, ix

call print_linebreak
ld a, $2b
call transmit_char
call wait

rerun_tests:

call print_linebreak

ld hl, test1_title
call prints

call random_init
ld hl, $1000

memtest_loop_1:
ld a, h
out ($f0), a
ld a, l
out ($f1), a

call random_next
ld b, a
ld (hl), a
ld a, (hl)
;call print_byte

cp b
jp z, memtest_continue_1

call dumpMemtestState
call halt_cpu

memtest_continue_1:

ld a, l
or a
jp nz, memtest_noprint_1

ld a, $2b
call transmit_char

memtest_noprint_1:
inc hl
ld a, h
or a
jp nz, memtest_loop_1
ld a, l
or a
jp nz, memtest_loop_1

ld a, $b0
out ($f0), a
ld a, $0b
out ($f1), a

ld a, $5e
call transmit_char

; -----------------------
ld hl, success_title
call prints

;repeat_dump:
;call dump_test_area
;call wait
;jr repeat_dump

; -----------------------

call print_linebreak
ld hl, test3_title
call prints

call random_init
ld hl, $1000

memtest_loop_3:
ld a, h
out ($f0), a
ld a, l
out ($f1), a

call random_next
ld b, a
ld a, (hl)
;call print_byte

cp b
jp z, memtest_continue_3

call dumpMemtestState
call halt_cpu

memtest_continue_3:

ld a, l
or a
jp nz, memtest_noprint_3

ld a, $2b
call transmit_char

memtest_noprint_3:
inc hl
ld a, h
or a
jp nz, memtest_loop_3
ld a, l
or a
jp nz, memtest_loop_3

ld a, $b0
out ($f0), a
ld a, $0b
out ($f1), a

ld a, $5e
call transmit_char

ld hl, success_title
call prints
call print_linebreak


call print_linebreak
ld hl, test4_title
call prints

ld hl, $1000

memtest_fill_loop_4:
ld (hl), l
inc hl
ld a, h
or a
jp nz, memtest_fill_loop_4
ld a, l
or a
jp nz, memtest_fill_loop_4


call random_init
ld hl, $1000

memtest_loop_4:

push hl
ld a, h
out ($f0), a
call random_next
ld l, a
out ($f1), a

ld b, a
ld a, (hl)
;call print_byte

cp b
jp z, memtest_continue_4

call dumpMemtestState
call halt_cpu

memtest_continue_4:

ld a, l
or a
jp nz, memtest_noprint_4

ld a, $2b
call transmit_char

memtest_noprint_4:
pop hl
inc hl
ld a, h
or a
jp nz, memtest_loop_4
ld a, l
or a
jp nz, memtest_loop_4

ld a, $b0
out ($f0), a
ld a, $0b
out ($f1), a

ld a, $5e
call transmit_char

ld hl, success_title
call prints
call print_linebreak


call print_linebreak
ld hl, test5_title
call prints

ld hl, $1000

memtest_fill_loop_5:
ld (hl), l
inc hl
ld a, h
or a
jp nz, memtest_fill_loop_5
ld a, l
or a
jp nz, memtest_fill_loop_5


call random_init
ld hl, $1000

memtest_loop_5:

push hl
call random_next
ld b, a
and $f0
jr nz, zero_region_skip_5
ld a, $10
or b
ld b, a
zero_region_skip_5:
ld a, b
ld h, b
out ($f0), a
ld a, l
out ($f1), a

ld b, a
ld a, (hl)
;call print_byte

cp b
jp z, memtest_continue_5

call dumpMemtestState
call print_linebreak
pop hl
ld a, h
call print_byte
ld a, l
call print_byte
call print_linebreak

call halt_cpu

memtest_continue_5:

ld a, l
or a
jp nz, memtest_noprint_5

ld a, $2b
call transmit_char

memtest_noprint_5:
pop hl
inc hl
ld a, h
or a
jp nz, memtest_loop_5
ld a, l
or a
jp nz, memtest_loop_5

ld a, $b0
out ($f0), a
ld a, $0b
out ($f1), a

ld a, $5e
call transmit_char

ld hl, success_title
call prints
call print_linebreak


ld hl, all_success_title
call prints
call print_linebreak

call clear_screen_ram
ld hl, all_success_title
call prints_screen

call wait
call wait
call wait
jp rerun_tests

include "debug_utils.asm"

dumpMemtestState:
    ld c, a
    call print_linebreak
    ld a, c
    call print_byte
    ld a, $20
    call transmit_char
    ld a, b
    call print_byte
    ld a, $20
    call transmit_char
    ld a, h
    call print_byte
    ld a, l
    call print_byte
    call print_linebreak

    ld c, $10

    dump_loop:
    ld a, $20
    call transmit_char
    ld a, (hl)
    call print_byte

    dec c
    inc hl
    or c
    jr nz, dump_loop
    ;call receive_char

    ret

dump_test_area:
    call print_linebreak
    ld hl, test2_title
    call prints

    ld hl, $1000

    dump_test_area_loop_2:
    ld a, h
    out ($f0), a
    ld a, l
    out ($f1), a

    call print_hexdump_line

    ld a, h
    or a
    jp nz, dump_test_area_loop_2
    ld a, l
    or a
    jp nz, dump_test_area_loop_2

    ret

random_init:
    push hl
    ld hl, random_seed
    ld (hl), $78
    pop hl
    ret

random_next:
    push hl
    ld hl, random_seed
    ld a, (hl)
    xor $d8
    add $55
    ld (hl), a
    pop hl
    ret

halt_cpu:
    call print_linebreak
    ld hl, cpu_halted
    call prints
    halt

clear_screen_ram:
    ld hl, $4000
    clear_screen_ram_loop:
    ld (hl), $00
    inc hl
    ld a, h
    cp $52
    jr nz, clear_screen_ram_loop
    ld a, l
    cp $c0
    jr nz, clear_screen_ram_loop
    ret

hex_chars: DEFB "0123456789abcdef"

test1_title: DEFB "Sequential read after write", 13, 10, 0
test2_title: DEFB "Memory dump", 13, 10, 0
test3_title: DEFB "Sequential re-read", 13, 10, 0
test4_title: DEFB "Random read (L)", 13, 10, 0
test5_title: DEFB "Random read (H)", 13, 10, 0
all_success_title: DEFB "ALL TESTS "
success_title: DEFB "PASS", 13, 10, 0
cpu_halted: DEFB "CPU Halted", 13, 10, 0

random_seed: DEFB $78
