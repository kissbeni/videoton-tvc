#!/bin/python

from PIL import Image

FONT_PATH = "../../roms/font.bin"

with open(FONT_PATH, "rb") as f:
    fontData = f.read()

fontCharacters = []

for x in range(len(fontData) // 8):
    chdata = [0]*8
    for y in range(8):
        chdata[y] = fontData[x * 8 + y]
    fontCharacters.append(bytes(chdata))

def compareCharacterData(d1, d2):
    andedBytes = bytes([a & b for a, b in zip(d1[::-1], d2[::-1])][::-1])

    n = 0
    for x in andedBytes:
        while x != 0:
            if x & 1:
                n += 1
            x = x >> 1
    return n

def findBestCharacterForBlock(blockData):
    bestScore = -1
    bestCharacter = -1

    for x in range(len(fontCharacters)):
        if x == 0x0a or x == 0x0d or x == 0x08 or x == 0x18:
            continue

        score = compareCharacterData(blockData, fontCharacters[x])
        if score > bestScore:
            bestScore = score
            bestCharacter = x

    return bestCharacter

for frameIdx in range(40, 6520):
    print("Frame %8d\r" % frameIdx, end="")
    img = Image.open('frames/ba-%08d.png' % frameIdx)
    pixels = img.load()

    targetWidth = img.size[0]//8
    targetHeight = img.size[1]//8

    with open('char-frames/ba-%08d.bin' % frameIdx, "wb") as outFile:
        for y in range(targetHeight):
            chars = []
            for x in range(targetWidth):
                blockData = []

                for oy in range(8):
                    b = 0
                    for ox in range(8):
                        pix = pixels[x*8 + ox, y*8 + oy]

                        if pix[0] > 180 or pix[1] > 180 or pix[2] > 180:
                            b = b | (1 << (7-ox))

                    blockData.append(b)

                bestChar = findBestCharacterForBlock(blockData)
                chars.append(bestChar)
                _chr = fontCharacters[bestChar]
                for chRow in range(8):
                    b = _chr[chRow]
                    for chCol in range(8):
                        pixels[x*8 + chCol, y*8 + chRow] = (255, 255, 255) if b & 128 else (0, 0, 0)
                        b = b << 1

            if y == targetHeight - 1:
                chars += [0]
            else:
                chars += [0,0]

            outFile.write(bytes(chars))

    img.save('char-frames/ba-%08d.png' % frameIdx)

print("")
