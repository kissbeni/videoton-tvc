#!/bin/python

import serial
import time
#from cv2 import *
import pyscreenshot as ImageGrab

serialPort = serial.Serial(port="/dev/ttyUSB0", baudrate=9600, bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)

def takeScreenshot(filename):
    im = ImageGrab.grab(bbox=(0,180,1920,1080))
    im.save(filename)

for frameIdx in range(40, 6520):
    print("Frame %8d\r" % frameIdx, end="")
    serialPort.write(b'\x18')
    time.sleep(0.3)
    with open('char-frames/ba-%08d.bin' % frameIdx, "rb") as f:
        serialPort.write(f.read())

    time.sleep(3)
    takeScreenshot("screen-frames/ba-%08d.png" % frameIdx)

print("")
serialPort.close()
