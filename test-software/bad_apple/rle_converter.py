#!/bin/python
import os
import sys

if len(sys.argv) != 2:
    print("Please provide frameskip")
    exit()

_PADDING = bytes()

FRAMESKIP = int(sys.argv[1])
END_FRAME = 6520
START_FRAME = 40

os.mkdir("char-frames/FS%d" % FRAMESKIP)

prevFrame = None

def calculateRleFrame(frameData, minimumLineLength = 1, maximumLineLength = 127):
    global prevFrame
    res = []

    prevByte = -1
    count = 0
    n = 0

    for x in thisFrame:
        if prevFrame is not None and prevFrame[n-count:] == thisFrame[n-count:]:
            count = 0
            break
        _byte = (x == 0xdb)
        if _byte != prevByte or count >= maximumLineLength:
            if count >= minimumLineLength:
                if count > 0:
                    res.append((0x80 if prevByte else 0) | count)
                    count = 1
            else:
                count += 1

            prevByte = _byte
        else:
            count += 1
        n += 1

    if count > 0:
        res.append((0x80 if prevByte else 0) | count)
    
    return res

lastFrameSize = 0
for frameIdx in range(START_FRAME, END_FRAME, FRAMESKIP):
    print("\rProcessing frame: %d of %d" % (frameIdx, END_FRAME), end='')

    with open('char-frames/ba-%08d.bin' % frameIdx, "rb") as f:
        thisFrame = f.read() + _PADDING

    bytesToSend = calculateRleFrame(thisFrame, 1, 125)
    frameSize = len(bytesToSend)
    if frameSize + lastFrameSize > 512:
        print("\nFrame %d is too big, removing short lines" % (frameIdx))
        bytesToSend = calculateRleFrame(thisFrame, 2, 125)
        frameSize = len(bytesToSend)
    prevFrame = thisFrame
    lastFrameSize = frameSize

    with open('char-frames/FS%d/ba-%08d.rle' % (FRAMESKIP, frameIdx), "wb") as f:
        f.write(bytes(bytesToSend))

