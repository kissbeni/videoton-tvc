# Bad Apple demo

People love playing bad apple on everything that has a screen... so here we are doing exactly that:)

This code uses the `screen_terminal.asm` test software for displaying the video. For capturing you can implement any kind of capture code in the `player.py` within the `takeScreenshot` function.

## Preparing the video

 - Get your hands on a video, let's call it `original.mp4`
 - Resize the video to fit inside *640x472* for example: `ffmpeg -i original.mp4 -vf scale=-1:472 resized.avi` (outputing to avi is required, because mp4s don't like arbitrary resolutions)
 - Create a folder named `frames` and extract every frame to it: `ffmpeg -i ba.avi frames/ba-%08d.png`
 - Edit `converter.py` to have the correct range of frame indexes on line *44*
 - Create two other folders named `screen-frames` and `char-frames`
 - Run `convert.py` prefferably with `pypy` (if you have infinite time at your hands, you can of course use the standard python interpreter, but it will be 4x slower)
 - Edit `player.py` to handle taking screenshots matching your setup, and to have the range of frames you want to play
 - Run `player.py` and wait:)
 - Recombine your screenshots to a video with `ffmpeg -framerate 30 -start_number X -i screen-frames/ba-%08d.png -c:v libx264 -r 30 output.mp4` (set *X* to your first frame index, and if needed replace *30* with your original video's framerate)

## Demo

[Watch it here](https://cloud.csumge.cc/s/3eA8Kb9TgHidKZ3)
