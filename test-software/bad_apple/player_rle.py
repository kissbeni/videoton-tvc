#!/bin/python

import serial
import time
import sys
import datetime
import math
import simpleaudio as sa
#import pyscreenshot as ImageGrab

serialPort = serial.Serial(port="/dev/ttyUSB1", baudrate=38400, bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)
serialPort.write(b'\xff\x18\x1b[2;2fRLE player: bad_apple')
wave_object = sa.WaveObject.from_wave_file('bad_apple.wav')

lastFrameTime = datetime.datetime.now()

time.sleep(2)
serialPort.write(b'\x1b[V') # switch to RLE mode

avgFps = 0
avgFramesize = 0
minFramesize = 999999
maxFramesize = -1
frameCount = 0
droppedCount = 0
totalBytesSent = 0

firstFrameFlag = True

def writeSerialPort(data):
    serialPort.write(bytes(data))
    #time.sleep(.0005)

play_object = wave_object.play()
time.sleep(1.4)
frameDrawTime = 0

FRAMESKIP = 2
END_FRAME = 6520
START_FRAME = 40

playBegin = datetime.datetime.now()
frameDrawBegin = datetime.datetime.now()
for frameIdx in range(START_FRAME, END_FRAME, FRAMESKIP):
    frameTimestamp = (frameIdx - START_FRAME) * (1/30)
    playerTimestamp = (datetime.datetime.now() - playBegin).total_seconds()
    
    # we are ahead, so we need to slow down a bit
    if playerTimestamp < frameTimestamp:
        time.sleep(frameTimestamp - playerTimestamp)
    
    # we are more than a frame behind, so we need to drop this frame, to catch up
    if playerTimestamp >= (frameTimestamp + (1/30)):
        droppedCount += 1
        frameDrawBegin = datetime.datetime.now()
        continue

    if not firstFrameFlag:
        frameCount += 1

    with open('char-frames/FS%d/ba-%08d.rle' % (FRAMESKIP, frameIdx), "rb") as f:
        thisFrame = f.read()

    if len(thisFrame) > 0:
        bytesToSend = bytes([0]) + thisFrame
        frameSize = len(bytesToSend)

        writeSerialPort(bytesToSend)
    else:
        frameSize = 0

    totalBytesSent += frameSize

    if not firstFrameFlag:
        currentFrameTime = datetime.datetime.now()
        fps = 1/(currentFrameTime - lastFrameTime).total_seconds()
        avgFps += fps
        avgFramesize += frameSize
        lastFrameTime = currentFrameTime
        maxFramesize = max(frameSize, maxFramesize)
        minFramesize = min(frameSize, minFramesize)
        print('\rFPS:%4.02f (AVG: %3.02f) FT: %1.04f Dropped: %d Frame: %5d FS: %4d (AVG: %4d) T: %.2f/%.2fs ' % (fps, avgFps / frameCount, frameDrawTime, droppedCount, frameIdx, frameSize, avgFramesize / frameCount, frameTimestamp, playerTimestamp), end="")
    else:
        firstFrameFlag = False

    frameDrawEnd = datetime.datetime.now()
    frameDrawTime = (frameDrawEnd - frameDrawBegin).total_seconds()

    sleepDelay = (1/(30/FRAMESKIP)) - frameDrawTime - 0.00005
    if sleepDelay > 0:
        time.sleep(sleepDelay)
    else:
        droppedCount += 1

    frameDrawBegin = datetime.datetime.now()

playtime = (datetime.datetime.now() - playBegin).total_seconds()
expectedPlaytime = (END_FRAME - START_FRAME) * (1/30)

time.sleep(1)
serialPort.write(b'\xff\x18') # exit RLE mode
serialPort.write((
    """\r
Last FPS: %.02f (AVG %.02f)\r\n
Frames rendered: %d (of %d)\r\n
Frames dropped: %d\r\n
Framesize: AVG: %d, MIN: %d, MAX: %d (bytes)\r\n
Data transmitted: %d bytes (%d kbytes) AVG Datarate: %.2f bps\r\n
Playtime: %d:%02d.%03d (Expected: %d:%02d.%03d)\r\n
""" % (
    fps,
    avgFps / frameCount,
    frameCount,
    END_FRAME - START_FRAME,
    droppedCount,
    avgFramesize / frameCount,
    minFramesize,
    maxFramesize,
    totalBytesSent,
    totalBytesSent // 1000,
    (totalBytesSent * 8) / playtime,
    playtime // 60,
    math.floor(playtime - ((playtime // 60)*60)),
    math.floor((playtime * 1000) % 1000),
    expectedPlaytime // 60,
    math.floor(expectedPlaytime - ((expectedPlaytime // 60)*60)),
    math.floor((expectedPlaytime * 1000) % 1000)
)).encode('utf-8'))

#play_object.wait_done()
print("")
serialPort.close()
