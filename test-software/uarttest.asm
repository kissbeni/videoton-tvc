
ld ix, $03f0
ld sp, ix

ld HL, helloMessage

txAsciiZMsg:
    ld  A, (HL)
    or  A
    jp Z, cont
    call transmit_char
    call wait2
    inc HL
    jr  txAsciiZMsg

cont:

ld c, $30

txNumbers:
    ld a, c
    cp $3a
    jr z, cont2
    call transmit_char
    inc c
    jr  txNumbers

cont2:

call print_linebreak
call receive_char
;ld b, a
call print_byte
;ld a, b
;call transmit_char

jp cont2

transmit_char:
    out ($f2), a
    out ($f1), a
    nop
    nop
    uart_wait:
    in a, ($F3)
    bit	0, a
    jp nz, uart_wait
    ret

receive_char:
	in	a, ($f3)
	bit	1, a
	jp	z, receive_char

    in a, ($f2)
    out ($f0), a
    ret

wait:
    push de
    ld DE, 0xFF00
    wait_inner1:
    dec DE
    ld A, D
    or E
    jp NZ, wait_inner1
    pop de
    ret

wait2:
    push de
    ld DE, 0x2500
    wait_inner2:
    dec DE
    ld A, D
    or E
    jp NZ, wait_inner2
    pop de
    ret

print_linebreak:
    ld a, 13
    call transmit_char
    ld a, 10
    call transmit_char
    ret

print_byte:
    push bc
    ld c, a
    call print_byte_a
    call transmit_char
    ld a, c
    call print_byte_b
    call transmit_char
    pop bc
    ret

print_byte_a:
    rra
    rra
    rra
    rra
print_byte_b:
    or $F0
    daa
    add a, $A0
    adc a, $40
    ret

helloMessage:
    DEFB "Hello from z80", 13, 10
abcdump:
    DEFB "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 13, 10, 0
