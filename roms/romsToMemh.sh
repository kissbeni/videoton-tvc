#!/bin/bash

ROMSDIR=$(dirname $(realpath $0))

# Character ROM
printf "%-512s" $(printf "%s" $(cat "$ROMSDIR/font.bin" | xxd -p)) | sed 's/  /00/g' | grep -oP "[a-z0-9]{16}" > "$ROMSDIR/charrom.txt"

# Debug ROM
printf "%-4096s" $(printf "%s" $(cat "$ROMSDIR/dbgrom.bin" | xxd -p)) | sed 's/  /00/g' | grep -oP "[a-z0-9]{2}" > "$ROMSDIR/dbgrom.txt"
