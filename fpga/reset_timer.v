`timescale 1ns / 1ps

module reset_timer(
    input  wire sysclk,
    input  wire dramclk,
    input  wire cpuclk,

    input  wire dirty_rst,

    output reg  sysrst,
    output reg  cpurst_n,
    output reg  dramrst,
    input  wire dram_ready,
    
    input  wire pll_locked,
    output reg  pll_reset
);

reg [12:0] counter = 0;
reg [7:0] cpuReleaseDelay = 0;

wire resetInitialDelayComplete = counter[12];

wire cpuclkRise;

clk_rise_detect cpuclk_rise_detect(
    .clk(sysclk),
    .inclk(cpuclk),
    .rise(cpuclkRise)
);

always @(posedge sysclk) begin
    if (~resetInitialDelayComplete) begin
        dramrst <= 1'b1;
        sysrst <= 1'b1;
        cpurst_n <= 1'b0;
        pll_reset <= 1'b1;
        cpuReleaseDelay <= 8'd190;
    end else begin
        pll_reset <= 1'b0;

        if (|cpuReleaseDelay) begin
            cpuReleaseDelay <= cpuReleaseDelay - 8'd1;
        end
        
        if (pll_locked) begin
            dramrst <= 1'b0;
        end
        
        if (dram_ready) begin
            if (~dramrst) begin
                sysrst <= 1'b0;
            end
            
            if (~|cpuReleaseDelay & ~sysrst & cpuclkRise) begin
                cpurst_n <= 1'b1;
            end
        end
    end
end

always @(posedge sysclk) begin
    if (dirty_rst) begin
        counter <= 0;
    end else if (~resetInitialDelayComplete) begin
        counter <= counter + 13'd1;
    end
end

endmodule
