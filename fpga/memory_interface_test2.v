`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   07:17:14 06/03/2022
// Design Name:   memory_interface
// Module Name:   X:/videoton-tvc/fpga/memory_interface_test2.v
// Project Name:  tvc-fpga
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: memory_interface
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module memory_interface_test2;

// Inputs
wire cpuclk;
wire dramclk;
wire sysclk;
reg rst;
reg clk_enable;
reg [15:0] addr;
reg [7:0] data_in;
reg wr;
reg rd;
reg memrq;
wire [13:0] pixDataAddr;
wire pixDataWrReq;
wire dramReady;
wire dramReadValid;
wire [7:0] dramReadData;

wire refresh_inhibit = 0;

// Outputs
wire [7:0] data_out;
wire out_data_valid;
wire [7:0] pixDataIn;
wire pixDataWr;
wire dramWriteRequest;
wire dramReadRequest;
wire [15:0] dramAddress;
wire [7:0] dramWriteData;


wire        vga_hsync;
wire        vga_vsync;
wire [ 4:0] vga_r;
wire [ 5:0] vga_g;
wire [ 4:0] vga_b;


wire  [11:0] sdram_addr;
wire  [ 1:0] sdram_ba;
wire         sdram_cke;
wire         sdram_clk;
wire         sdram_cs_n;
wire         sdram_dqmh;
wire         sdram_dqml;
wire         sdram_we_n;
wire         sdram_cas_n;
wire         sdram_ras_n;
wire [15:0]  sdram_dq;

sdram_controller _sdram_controller(
    sdram_addr,
    sdram_ba,
    sdram_cke,
    sdram_clk,
    sdram_cs_n,
    sdram_dqmh,
    sdram_dqml,
    sdram_we_n,
    sdram_cas_n,
    sdram_ras_n,
    sdram_dq,

    dramReady,
    dramWriteRequest,
    dramReadRequest,
    dramReadValid,

    dramclk,
    rst,
    refresh_inhibit,

    dramAddress,
    dramWriteData,
    dramReadData
);

wire [1:0] Dqm = {sdram_dqmh, sdram_dqml};
sdr SDRAMIC(
    sdram_dq,
    sdram_addr,
    sdram_ba,
    sdram_clk,
    sdram_cke,
    sdram_cs_n,
    sdram_ras_n,
    sdram_cas_n,
    sdram_we_n,
    Dqm
);

vga_controller vga(
    .clk(sysclk),
    .rst(rst),
    .vga_hsync(vga_hsync),
    .vga_vsync(vga_vsync),
    .vga_r(vga_r),
    .vga_g(vga_g),
    .vga_b(vga_b),
    .pixDataIn(pixDataIn),
    .pixDataWr(pixDataWr),
    .pixDataAddr(pixDataAddr),
    .pixDataWrReq(pixDataWrReq)
);

// Instantiate the Unit Under Test (UUT)
memory_interface uut (
    .sysclk(sysclk), 
    .dramclk(dramclk), 
    .rst(rst), 
    .addr(addr), 
    .data_in(data_in), 
    .data_out(data_out), 
    .wr(wr), 
    .rd(rd), 
    .memrq(memrq), 
    .out_data_valid(out_data_valid), 
    .pixDataIn(pixDataIn), 
    .pixDataWr(pixDataWr), 
    .pixDataAddr(pixDataAddr), 
    .pixDataWrReq(pixDataWrReq), 
    .dramReady(dramReady), 
    .dramWriteRequest(dramWriteRequest), 
    .dramReadRequest(dramReadRequest), 
    .dramReadValid(dramReadValid), 
    .dramAddress(dramAddress), 
    .dramWriteData(dramWriteData), 
    .dramReadData(dramReadData)
);

clock_gen #(.FREQ(100_000)) u1(clk_enable, dramclk);

reg [4:0] clkdivctr = 0;
always @(posedge dramclk) begin
    clkdivctr <= clkdivctr + 5'd1;
end

assign cpuclk = clkdivctr[3];
assign sysclk = clkdivctr[0];

initial begin
    // Initialize Inputs
    rst <= 1;
    clk_enable <= 1;
    addr <= 0;
    data_in <= 0;
    wr <= 0;
    rd <= 0;
    memrq <= 0;
    
    // Wait 100 ns for global reset to finish
    #100;
    rst <= 0;

    // Add stimulus here
    @(negedge cpuclk);
    @(negedge cpuclk);
    @(negedge cpuclk);
    @(negedge cpuclk);
    @(negedge cpuclk); #1;

    memrq <= 1'b1;
    addr <= 16'hff00;
    data_in <= 8'hAF;

    @(negedge cpuclk); #1;
    wr <= 1'b1;
    @(negedge cpuclk); #1;

    memrq <= 1'b0;
    wr <= 1'b0;
    addr <= 16'h0000;

    @(negedge cpuclk);  #1;

    memrq <= 1'b1;
    rd <= 1'b1;
    addr <= 16'hff00;

    @(negedge cpuclk); #1;
    @(negedge cpuclk); #1;

    memrq <= 1'b0;
    rd <= 1'b0;
    addr <= 16'h0000;

    @(negedge cpuclk); #1;
end

endmodule

