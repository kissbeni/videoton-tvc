`timescale 1ns / 1ps

module topm(
    input  wire        sysclk,
    input  wire [ 3:0] bt,
    input  wire        uart_rx,
    output wire [ 7:0] ssd_seg,
    output wire [ 3:0] ssd_dig,
    output wire [ 3:0] ld,
    output wire        uart_tx,
    output wire [12:0] sdram_addr,
    output wire [ 1:0] sdram_bank,
    output wire        sdram_cke,
    output wire        sdram_clk,
    output wire        sdram_cs_n,
    output wire [ 1:0] sdram_dqm,
    output wire        sdram_we_n,
    output wire        sdram_cas_n,
    output wire        sdram_ras_n,
    inout  wire [15:0] sdram_data,
    output wire        vga_hsync,
    output wire        vga_vsync,
    output wire [ 4:0] vga_r,
    output wire [ 5:0] vga_g,
    output wire [ 4:0] vga_b,
    output wire        buzzer
);

wire rst;
wire n_cpurst;
wire cpuclk;
wire dramclk;

wire pll_locked;
wire pll_reset;
wire clockfb;

clockgen ckg(
    .sysclk(sysclk),    // 50MHz system clock
    .cpuclk(cpuclk),    // 3.125MHz CPU clock
    .dramclk(dramclk),  // 100MHz SDRAM clock
    
    .pll_locked(pll_locked),
    .pll_reset(pll_reset),
    
    .fbin(clockfb),
    .fbout(clockfb)
);

always @(posedge cpuclk) begin
    n_int <= bt[2];
    n_nmi <= bt[3];
end

wire cpuclkRise;

clk_rise_detect cpuclk_rise_detect(
    .clk(sysclk),
    .inclk(cpuclk),
    .rise(cpuclkRise)
);

wire n_iorq;
wire n_memrq;
wire n_rd;
wire n_wr;
reg n_int;
reg n_nmi;

wire iorq = ~n_iorq;
wire memrq = ~n_memrq;
wire rd = ~n_rd;
wire wr = ~n_wr;

wire [7:0] readData;

wire [15:0] addr;
wire [7:0]  data;

wire memio_data_valid;
wire [7:0] memio_data;

assign data = memio_data_valid ? memio_data : (rd ? readData : 8'dzzzz);

z80_top_direct_n cpu(
    .nMREQ(n_memrq),
    .nIORQ(n_iorq),
    .nRD(n_rd),
    .nWR(n_wr),
    .nWAIT(1'b1),
    .nINT(n_int),
    .nNMI(n_nmi),
    .nRESET(n_cpurst),
    .nBUSRQ(1'b1),
    .nRFSH(),
    .nBUSACK(),
    .nHALT(ld[0]),
    .nM1(),

    .CLK(cpuclk),
    .A(addr),
    .D(data)
);

ioport_controller io(
    .sysclk(sysclk),
    .cpuclk(cpuclk),
    .rst(rst),
    .addr(addr[7:0]),
    .data_in(data),
    .data_out(readData),
    .rd(rd),
    .wr(wr),
    .iorq(iorq),
    .cpuclkRise(cpuclkRise),
    .bt(bt[1]),
    .ld(ld[3:1]),
    .uart_rx(uart_rx),
    .uart_tx(uart_tx),
    .ssd_seg(ssd_seg),
    .ssd_dig(ssd_dig),
    .sound(buzzer)
);

wire [7:0]  pixDataIn;
wire        pixDataWr;
wire [13:0] pixDataAddr;
wire        pixDataWrReq;

wire [11:0] _sdram_addr;
assign sdram_addr = {1'd0, _sdram_addr};

wire dramReady;
reg  dramReadyBuf;
wire dramWriteRequest;
wire dramReadRequest;
wire dramReadValid;
wire dramReset;
wire [15:0] dramAddress;
wire [7:0] dramWriteData;
wire [7:0] dramReadData;

memory_interface memio(
    .sysclk(sysclk),
    .dramclk(dramclk),
    .rst(rst),
    .addr(addr),
    .data_in(data),
    .data_out(memio_data),
    .wr(wr),
    .rd(rd),
    .memrq(memrq),
    .out_data_valid(memio_data_valid),
    .pixDataIn(pixDataIn),
    .pixDataWr(pixDataWr),
    .pixDataAddr(pixDataAddr),
    .pixDataWrReq(pixDataWrReq),
    .dramReady(dramReadyBuf),
    .dramWriteRequest(dramWriteRequest),
    .dramReadRequest(dramReadRequest),
    .dramReadValid(dramReadValid),
    .dramAddress(dramAddress),
    .dramWriteData(dramWriteData),
    .dramReadData(dramReadData)
);

sdram_controller sdram(
    .sdram_addr(_sdram_addr),
    .sdram_ba(sdram_bank),
    .sdram_cke(sdram_cke),
    .sdram_clk(sdram_clk),
    .sdram_cs_n(sdram_cs_n),
    .sdram_dqmh(sdram_dqm[0]),
    .sdram_dqml(sdram_dqm[1]),
    .sdram_we_n(sdram_we_n),
    .sdram_cas_n(sdram_cas_n),
    .sdram_ras_n(sdram_ras_n),
    .sdram_dq(sdram_data),

    .ready(dramReady),
    .write_req(dramWriteRequest),
    .read_req(dramReadRequest),
    .out_valid(dramReadValid),
    .dramclk(dramclk),
    .rst(dramReset),
    
    .refresh_inhibit(memrq),

    .in_address({4'd0, dramAddress}),
    .in_data(dramWriteData),
    .out_data(dramReadData)
);

always @(posedge sysclk) begin
    dramReadyBuf <= dramReady;
end

vga_controller vga(
    .clk(sysclk),
    .rst(rst),
    .vga_hsync(vga_hsync),
    .vga_vsync(vga_vsync),
    .vga_r(vga_r),
    .vga_g(vga_g),
    .vga_b(vga_b),
    .pixDataIn(pixDataIn),
    .pixDataWr(pixDataWr),
    .pixDataAddr(pixDataAddr),
    .pixDataWrReq(pixDataWrReq)
);

reset_timer rsttimer(
    .sysclk(sysclk),
    .dramclk(dramclk),
    .cpuclk(cpuclk),

    .dirty_rst(~bt[0]),
    
    .sysrst(rst),
    .cpurst_n(n_cpurst),
    .dramrst(dramReset),
    .dram_ready(dramReadyBuf),
    
    .pll_locked(pll_locked),
    .pll_reset(pll_reset)
);

endmodule
