`timescale 1ns / 1ps

module fall_edge_latch(
    input  wire clk,
    input  wire in,
    output wire trigger,
    input  wire ack
);

localparam [1:0] STATE_ARMED = 2'd0;
localparam [1:0] STATE_TRIGGERED = 2'd1;
localparam [1:0] STATE_WAIT_FALL = 2'd2;

reg [1:0] state;

always @(posedge clk) begin
    case (state)
        STATE_ARMED: 	 if (~in)   state <= STATE_TRIGGERED;
        STATE_TRIGGERED: if (ack)   state <= STATE_WAIT_FALL;
        STATE_WAIT_FALL: if (in)    state <= STATE_ARMED;
        default:                    state <= STATE_ARMED;
    endcase
end

assign trigger = (state == STATE_TRIGGERED);

endmodule
