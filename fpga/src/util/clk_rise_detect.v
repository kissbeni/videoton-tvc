`timescale 1ns / 1ps

module clk_rise_detect(
    input  wire clk,
    input  wire inclk,
    output wire rise
);

reg sample;

always @(posedge inclk) begin
    sample <= ~sample;
end

reg [1:0] sample_buf;

always @(posedge clk) begin
    sample_buf <= {sample_buf[0], sample};
end

assign rise = ^sample_buf;

endmodule

/*
`timescale 1ns / 1ps

module clk_rise_detect(
    input  wire clk,
    input  wire inclk,
    output wire rise
);

reg [1:0] sample_buf;

always @(posedge clk) begin
    sample_buf <= {sample_buf[0], inclk};
end

assign rise = sample_buf == 2'b01;

endmodule
*/
