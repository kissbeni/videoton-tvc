`timescale 1ns / 1ps

module ps2_interface(
    input  wire         sysclk,
    input  wire         rst,
    input  wire         ps2_clk,
    input  wire         ps2_data,
    output reg  [ 7:0]  data_out,
    output reg          data_valid
);

wire sampleInputData;
rise_edge_latch sample_edge_detector(clk, ps2_clk, sampleInputData, sampleInputData);

//    10   ,   9   ,  8:1    ,    0
// stop bit, parity, data (8), start bit
reg [10:0] data_sample_sr;

always @(posedge sysclk) begin
    if (sampleInputData)
        data_sample_sr <= {ps2_data, data_sample_sr[9:0]};
end

assign data_parity = ^data_sample_sr[9:2];
assign data_valid_w =
            data_sample_sr[10]                  // Stop bit is 1
          & (data_parity == data_sample_sr[9])  // Parity is correct
          & ~data_sample_sr[0];                 // Start bit is 0

always @(posedge sysclk) begin
    if (sampleInputData) data_valid <= 1'b0;
    else if (data_valid_w) begin
        out_data <= data_sample_sr[8:1];
        data_valid <= 1'b1;
    end
end

endmodule
