`timescale 1ns / 1ps

module audio_intf(
    input  wire       sysclk,
    input  wire       cpuclkRise,
    input  wire       rst,
    input  wire [7:0] data,
    input  wire       wr,
    input  wire [1:0] regsel,
    output wire       sound,
    output wire       sdint,
    output wire       txrxclk,
    input  wire       instart
);

reg [7:0] topReg;
reg [7:0] bottomReg;
reg [3:0] volumeReg;

always @(posedge sysclk) begin
    if (rst) begin
        topReg <= 8'd0;
        bottomReg <= 8'd0;
        volumeReg <= 4'd0;
    end else if (wr) begin
             if (regsel == 2'd0) topReg <= data;
        else if (regsel == 2'd1) bottomReg <= data;
        else if (regsel == 2'd2) volumeReg <= data[5:2];
    end
end

wire [11:0] counterInitialValue = {bottomReg[3:0], topReg};
wire        soundEnable = bottomReg[4];
reg  [11:0] counterValue;
reg  [ 3:0] divCounterValue;
wire        audio_load = instart | (counterValue == 12'b1111_1111_1111);

assign sdint = divCounterValue[3] & bottomReg[5];
assign sound = divCounterValue[3] & soundEnable & |volumeReg;
assign txrxclk = divCounterValue[0];

always @(posedge sysclk) begin
    if (cpuclkRise) begin
        if (audio_load)
            counterValue <= counterInitialValue;
        else
            counterValue <= counterValue + 12'd1;
    end
end

wire audioLoadTrigger;
rise_edge_latch audio_load_rise(sysclk, audio_load, audioLoadTrigger, audioLoadTrigger);

always @(posedge sysclk) begin
    if (instart)
        divCounterValue <= 4'd0;
    else if (audioLoadTrigger)
        divCounterValue <= divCounterValue + 4'd1;
end

endmodule
