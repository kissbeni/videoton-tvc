`timescale 1ns / 1ps

module uart_rx(
    input  wire       clk,
    input  wire       rst,
    input  wire       uart_rx,
    output reg  [7:0] data,
    output reg        valid
);

`include "../../params.vh"

localparam [12:0] BITRATE_DIV = UART_BITRATE_DIV;
localparam [12:0] ONEHALF_BITRATE_DIV = BITRATE_DIV + (BITRATE_DIV/2);

reg [12:0] div_counter;
reg run;
reg firstBitFlag;

reg [3:0] sampleCounter;
reg [9:0] sampleSr;

wire rx_trigger = div_counter == (firstBitFlag ? ONEHALF_BITRATE_DIV : BITRATE_DIV);

// bit timing
always @(posedge clk) begin
    if (rst | ~run) begin
        firstBitFlag <= 1'b1;
        div_counter <= 13'd0;
    end
    else if (rx_trigger) begin
        div_counter <= 13'd0;
        firstBitFlag <= 1'b0;
    end else
        div_counter <= div_counter + 13'd1;
end

reg [1:0] sample;

// input sampler
always @(posedge clk) begin
    sample <= {sample[0], uart_rx};
end

// start condition detector
always @(posedge clk) begin
    if (rst) begin
        run <= 1'b0;
        data <= 8'd0;
    end else begin
        if (~run & sample == 2'b10) // start condition
            run <= 1'b1;
        if (run & sampleCounter == 4'd9) begin
            run <= 1'b0;
            data <= sampleSr[8:1];
        end
    end
end

always @(posedge clk) begin
    if (rst) begin
        sampleCounter <= 4'd0;
        valid <= 1'b0;
    end else begin
        if (valid) valid <= 1'b0;
        if (~run) begin
            sampleSr <= 10'd0;
            sampleCounter <= 4'd0;
            valid <= (sampleSr[0] == 1'b0 & sampleSr[9] == 1'b1);
        end
        if (run & rx_trigger) begin
            sampleSr <= {sample[0], sampleSr[9:1]};
            sampleCounter <= sampleCounter + 4'd1;
        end
    end
end

endmodule
