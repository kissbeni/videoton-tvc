module uart_tx(
    input  wire        clk,
    input  wire        rst,
    input  wire        wr_start,
    input  wire [ 7:0] data,
    output reg         uart_busy,
    output wire        uart_tx
);

`include "../../params.vh"

reg [12:0] div_counter;
reg [10:0] dataSr;
reg [ 3:0] bitCounter;
reg        run;

wire tx_trigger = div_counter == UART_BITRATE_DIV;

always @(posedge clk) begin
    if (rst | tx_trigger | ~run)
        div_counter <= 12'd0;
    else
        div_counter <= div_counter + 13'd1;
end

assign uart_tx = dataSr[0];

always @(posedge clk) begin
    uart_busy <= run | wr_start;
end

always @(posedge clk) begin
    if (rst) begin
        dataSr <= 11'b11111111111;
        run <= 1'b1;
        bitCounter <= 4'd0;
    end
    else if (~run) begin
        if (wr_start) begin
            run <= 1'b1;
            dataSr <= {1'b1, data, 1'b0, 1'b1};
            bitCounter <= 4'd0;
        end
    end
    else if (tx_trigger) begin
        dataSr <= {1'b1, dataSr[9:1]};
        bitCounter <= bitCounter + 4'd1;
        if (bitCounter == 4'd11) begin
            run <= 1'b0;
        end
    end
end

endmodule
