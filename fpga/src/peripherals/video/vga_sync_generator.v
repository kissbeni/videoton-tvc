`timescale 1ns / 1ps

module vga_sync_generator(
    input  wire      clk,
    input  wire      cke,
    output reg       vga_hsync = 1'b0,
    output reg       vga_vsync = 1'b0,
    output reg       paper = 1'b0,
     output reg       newFrame = 1'b0,
    output reg [9:0] x_counter = 10'd0,
    output reg [9:0] y_counter = 10'd0
);

wire xEnd = (x_counter == 800); // 16 + 48 + 96 + 640
wire yEnd = (y_counter == 525); // 10 + 2 + 33 + 480

always @(posedge clk) begin
    if (newFrame) newFrame <= 1'b0;
    if (cke) begin
        if (xEnd) begin
            x_counter <= 10'd0;

            if (yEnd) begin
                    y_counter <= 10'd0;
                    newFrame <= 1'b1;
                end else begin
                    y_counter <= y_counter + 10'd1;
                end
        end else
            x_counter <= x_counter + 10'd1;
    end
end

always @(posedge clk) begin
    if (cke) begin
        vga_hsync <= ~(((x_counter > (640 + 16)) & (x_counter < (640 + 16 + 96))));   // active for 96 clocks
        vga_vsync <= ~(((y_counter > (480 + 10)) & (y_counter < (480 + 10 + 2))));   // active for 2 clocks

        paper <= (x_counter < 640) & (y_counter < 480);
    end
end

endmodule
