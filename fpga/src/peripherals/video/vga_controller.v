`timescale 1ns / 1ps

module vga_controller(
    input  wire        clk,
    input  wire        rst,
    output wire        vga_hsync,
    output wire        vga_vsync,
    output wire [ 4:0] vga_r,
    output wire [ 5:0] vga_g,
    output wire [ 4:0] vga_b,

    input  wire [ 7:0] pixDataIn,
    input  wire        pixDataWr,
    output wire [13:0] pixDataAddr,
    output reg         pixDataWrReq = 1'b0
);

reg [0:63] characterRom [255:0];

initial begin
    $readmemh("X:/videoton-tvc/roms/charrom.txt", characterRom, 0, 255);
end

reg [6:0] lineRegisterWritePos;
wire [6:0] characterCounter;
wire [7:0] characterIndex;
wire lineRegisterSelect;
wire [9:0] y_counter;
wire [7:0] colorData;

wire [6:0] character_row = y_counter[9:3];

wire lineBufStore;
rise_edge_latch linebuf_store_latch(clk, pixDataWr, lineBufStore, lineBufStore);

wire [7:0] pixColorDataIn = pixDataAddr[7:0];

reg [7:0] pixDataBuf;
reg [12:0] pixDataAddrBuf;
reg readingColor;

assign pixDataAddr = {readingColor, pixDataAddrBuf};

video_line_buffer linebuf(
    .sysclk(clk),
    
    .writeAddr(lineRegisterWritePos),
    .writeData(pixDataBuf),
    .colorData(pixDataIn),
    .store(lineBufStore & readingColor),
    
    .readAddr(characterCounter),
    .readData(characterIndex),
    .readColorData(colorData),
    
    .lineRegisterSelect(lineRegisterSelect)
);

reg lineRegisterSelectPrev;

wire newFrame;

always @(posedge clk) begin
    if (rst) begin
        readingColor <= 1'b0;
    end else if (lineBufStore) begin
        readingColor <= ~readingColor;
        if (~readingColor)
            pixDataBuf <= pixDataIn;
    end
end

always @(posedge clk) begin
    if (rst) begin
        pixDataAddrBuf <= 13'd0;
        pixDataWrReq <= 1'b0;
        lineRegisterWritePos <= 7'd0;
        lineRegisterSelectPrev <= 1'b0;
    end
    else begin
        if ((character_row < 10'd60 | character_row == 10'd65) & lineRegisterWritePos < 7'd80 & ~pixDataWr) begin
            pixDataWrReq <= 1'b1;
        end
        
        if (character_row == 10'd64) begin
            pixDataAddrBuf <= 13'd0;
        end
        
        if (lineRegisterSelectPrev != lineRegisterSelect) begin
            lineRegisterWritePos <= 7'd0;
        end
        
        if (lineBufStore) pixDataWrReq <= 1'b0;
        
        if (lineBufStore & readingColor) begin
            lineRegisterWritePos <= lineRegisterWritePos + 7'd1;
            pixDataAddrBuf <= pixDataAddrBuf + 13'd1;
        end
        
        lineRegisterSelectPrev <= lineRegisterSelect;
    end
end

reg clken = 1'b0;
wire paper;
wire [9:0] x_counter;

assign lineRegisterSelect = y_counter[3];
assign characterCounter = x_counter[9:3];

// wire [7:0] characterIndex = x_counter[9:3] + y_counter[9:3] * 80;

wire [0:63] currentChar = characterRom[characterIndex];

reg [0:7] _currentCharLine;
wire [0:7] currentCharLine = {_currentCharLine[7], _currentCharLine[0:6]};

always @(*) begin
    case (y_counter[2:0])
        3'd0: _currentCharLine <= currentChar[0:7];
        3'd1: _currentCharLine <= currentChar[8:15];
        3'd2: _currentCharLine <= currentChar[16:23];
        3'd3: _currentCharLine <= currentChar[24:31];
        3'd4: _currentCharLine <= currentChar[32:39];
        3'd5: _currentCharLine <= currentChar[40:47];
        3'd6: _currentCharLine <= currentChar[48:55];
        3'd7: _currentCharLine <= currentChar[56:63];
    endcase
end

always @(posedge clk) begin
    clken <= ~clken;
end

reg pix;

wire        fg_red   = colorData[7];
wire [ 1:0] fg_green = colorData[6:5];
wire        fg_blue  = colorData[4];

wire        bg_red   = colorData[3];
wire [ 1:0] bg_green = colorData[2:1];
wire        bg_blue  = colorData[0];

wire [4:0] padded_r = (pix & fg_red) | (~pix & bg_red) ? 5'b11111 : 5'd0;
wire [5:0] padded_g = pix ? {fg_green, 4'b1111 * |fg_green}  : {bg_green, 4'b1111 * |bg_green};
wire [4:0] padded_b = (pix & fg_blue) | (~pix & bg_blue) ? 5'b11111 : 5'd0;

assign vga_r = paper ? padded_r : 5'd0;
assign vga_g = paper ? padded_g : 6'd0;
assign vga_b = paper ? padded_b : 5'd0;

/*assign vga_r = 5'd0;
assign vga_b = 5'd0;

reg pix;
assign vga_g = pix ? 6'b111111 : 6'd0;*/

vga_sync_generator syncgen(
    .clk(clk),
    .cke(clken),
    .vga_hsync(vga_hsync),
    .vga_vsync(vga_vsync),
    .paper(paper),
    .newFrame(newFrame),
    .x_counter(x_counter),
    .y_counter(y_counter)
);

always @(posedge clk) begin
    if (paper) begin
        pix <= currentCharLine[x_counter[2:0]];
    end else begin
        pix <= 1'b0;
    end
end

endmodule
