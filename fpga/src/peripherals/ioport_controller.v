`timescale 1ns / 1ps

module ioport_controller(
    input  wire       sysclk,
    input  wire       cpuclk,
    input  wire       rst,
    input  wire [7:0] addr,
    input  wire [7:0] data_in,
    output reg  [7:0] data_out,
    input  wire       rd,
    input  wire       wr,
    input  wire       iorq,
    input  wire       cpuclkRise,
    input  wire       bt,
    output wire [2:0] ld,
    input  wire       uart_rx,
    output wire       uart_tx,
    output wire [7:0] ssd_seg,
    output wire [3:0] ssd_dig,
    output wire       sound
);

// TVC ports:
localparam [7:0] PORT_AUDIO_TOP       = 'h04;
localparam [7:0] PORT_AUDIO_BOTTOM    = 'h05;
localparam [7:0] PORT_AUDIO_VOLUME    = 'h06;
localparam [7:0] PORT_AUDIO_LOAD      = 'h5b;
localparam [7:0] PORT_AUDIO_LOAD_ALT  = 'h5f;   // The documentation gives port 5B, but the circuit doesn't care about A2, so 5F is also valid

// Testing ports:
localparam [7:0] PORT_7SEG_TOP    = 'hf1;
localparam [7:0] PORT_7SEG_BOTTOM = 'hf0;
localparam [7:0] PORT_UART_RXTX   = 'hf2;
localparam [7:0] PORT_SFR         = 'hf3;

wire        ioPortSelectTrigger;
wire        rx_fifo_empty;
wire        rx_fifo_almost_full;
reg         tx_start;
reg  [ 7:0] tx_data;
wire [ 7:0] rx_data;
wire [ 7:0] rx_data_buf;
wire        rx_data_valid;
wire        sfr_tx_busy;
wire        sfr_rx_ready;
reg  [ 7:0] displData [1:0];

assign sfr_rx_ready = ~rx_fifo_empty;

assign ld[2] = ~sfr_tx_busy;
assign ld[1] = ~sfr_rx_ready;
assign ld[0] = ~rx_fifo_almost_full;

// status flag register
wire [7:0] sfr = {4'd0, rx_fifo_almost_full, sfr_rx_ready, sfr_tx_busy};

rise_edge_latch io_port_select_latch(sysclk, (rd | wr) & iorq, ioPortSelectTrigger, ioPortSelectTrigger & cpuclkRise);

wire ioPortReadSelect  = ioPortSelectTrigger & rd & cpuclkRise;
wire ioPortWriteSelect = ioPortSelectTrigger & wr & cpuclkRise;

// Read handler
always @(posedge sysclk) begin
    if (rst) begin
        data_out <= 8'd0;
    end
    else if (ioPortReadSelect) begin
        if (addr == PORT_SFR)
            data_out <= sfr;
        else if (addr == PORT_UART_RXTX) begin
            data_out <= rx_data_buf;
        end
    end
end

// IO port write
always @(posedge sysclk) begin
    if (tx_start) tx_start <= 1'b0;
    if (rst) begin
        tx_start <= 1'b0;
        displData[0] <= 8'd0;
        displData[1] <= 8'd0;
    end
    else if (ioPortWriteSelect) begin
             if (addr == PORT_7SEG_BOTTOM) displData[0] <= data_in;
        else if (addr == PORT_7SEG_TOP)    displData[1] <= data_in;
        else if (addr == PORT_UART_RXTX) begin
            tx_data <= data_in;
            tx_start <= 1;
        end
    end
end

wire audio_sel = (addr == PORT_AUDIO_TOP) | (addr == PORT_AUDIO_BOTTOM) | (addr == PORT_AUDIO_VOLUME);
reg [1:0] audio_regsel;

always @(*) begin
    case (addr)
        PORT_AUDIO_TOP: audio_regsel <= 2'd0;
        PORT_AUDIO_BOTTOM: audio_regsel <= 2'd1;
        PORT_AUDIO_VOLUME: audio_regsel <= 2'd2;
        default: audio_regsel <= 2'd3;
    endcase
end

audio_intf audio(
    .sysclk(sysclk),
    .cpuclkRise(cpuclkRise),
    .rst(rst),
    .data(data_in),
    .wr(ioPortWriteSelect & audio_sel),
    .regsel(audio_regsel),
    .sound(sound),
    .sdint(),
    .txrxclk(),
    .instart(ioPortReadSelect & ((addr == PORT_AUDIO_LOAD) | (addr == PORT_AUDIO_LOAD_ALT)))
);

uart_rx_fifo rx_fifo(
    .sysclk(sysclk),
    .rst(rst),
    .take(ioPortReadSelect & (addr == PORT_UART_RXTX)),
    .put(rx_data_valid),
    .empty(rx_fifo_empty),
    .full(),
    .almost_full(rx_fifo_almost_full),
    .data_out(rx_data_buf),
    .data_in(rx_data)
);

uart_tx utx(
    .clk(sysclk),
    .rst(rst),
    .uart_busy(sfr_tx_busy),
    .uart_tx(uart_tx),
    .wr_start(tx_start),
    .data(tx_data)
);

uart_rx urx(
    .clk(sysclk),
    .rst(rst),
    .uart_rx(uart_rx),
    .data(rx_data),
    .valid(rx_data_valid)
);

ssd sd(
    sysclk,
    4'd0,
    bt ? {displData[0], displData[1]} : addr,
    ssd_seg,
    ssd_dig
);

endmodule
