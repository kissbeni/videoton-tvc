`timescale 1ns / 1ps

module ssd(
    input  wire        clk,
    input  wire [ 3:0] dp,
    input  wire [15:0] data,
    output reg  [ 7:0] seg,
    output wire [ 3:0] dig
);

reg [17:0] count;

wire cle = ~|count;
always @(posedge clk)
    count <= count + 18'b1;

reg [1:0] displ;
reg [3:0] num;

always @(posedge clk)
    if (cle) displ <= displ + 2'b1;

assign dig = ~(4'b1 << displ);

wire dp_en = dp[displ];

always @(*) begin
    case (displ)
        2'b00: 	num <= data[3:0];
        2'b01: 	num <= data[7:4];
        2'b10: 	num <= data[11:8];
        2'b11: 	num <= data[15:12];
    endcase
end

always @(*) begin
    case (num)
        4'h0:     seg <= {dp_en,7'b0111111};
        4'h1:     seg <= {dp_en,7'b0000110};
        4'h2:     seg <= {dp_en,7'b1011011};
        4'h3:     seg <= {dp_en,7'b1001111};
        4'h4:     seg <= {dp_en,7'b1100110};
        4'h5:     seg <= {dp_en,7'b1101101};
        4'h6:     seg <= {dp_en,7'b1111101};
        4'h7:     seg <= {dp_en,7'b0000111};
        4'h8:     seg <= {dp_en,7'b1111111};
        4'h9:     seg <= {dp_en,7'b1101111};
        4'hA:     seg <= {dp_en,7'b1110111};
        4'hB:     seg <= {dp_en,7'b1111100};
        4'hC:     seg <= {dp_en,7'b0111001};
        4'hD:     seg <= {dp_en,7'b1011110};
        4'hE:     seg <= {dp_en,7'b1111001};
        4'hF:     seg <= {dp_en,7'b1110001};
    endcase
end

endmodule
