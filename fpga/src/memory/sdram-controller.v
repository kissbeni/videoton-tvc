
module sdram_controller (
    output wire [11:0] sdram_addr,
    output wire [ 1:0] sdram_ba,
    output wire        sdram_cke,
    output wire        sdram_clk,
    output wire        sdram_cs_n,
    output wire        sdram_dqmh,
    output wire        sdram_dqml,
    output wire        sdram_we_n,
    output wire        sdram_cas_n,
    output wire        sdram_ras_n,
    inout  wire [15:0] sdram_dq,

    output wire        ready,

    input  wire        write_req,
    input  wire        read_req,
    output reg         out_valid,

    input  wire        dramclk,
    input  wire        rst,

    input  wire        refresh_inhibit,

    input  wire [19:0] in_address,
    input  wire [ 7:0] in_data,
    output reg  [ 7:0] out_data
);

// bit order is RAS CAS WE
localparam [2:0] SDRAM_CMD_MODE_REG_SET  = 3'b000;
localparam [2:0] SDRAM_CMD_NOP           = 3'b111;
localparam [2:0] SDRAM_CMD_PRECHARGE_ALL = 3'b010;
localparam [2:0] SDRAM_CMD_BANK_ACTIVE   = 3'b011;
localparam [2:0] SDRAM_CMD_WRITE         = 3'b100;
localparam [2:0] SDRAM_CMD_READ          = 3'b101;
localparam [2:0] SDRAM_CMD_REFRESH       = 3'b001;

// Reserved                                     (00)
// Write mode      -> Burst read, single write  (1)
// Reserved                                     (0)
// Test mode       -> off                       (0)
// CAS Latency     -> 2                         (010)
// Addressing mode -> Sequential                (1)
// Burst length    -> 1                         (000)
parameter SDRAM_SETTINGS = 12'b00_1_0_0_010_0_000;

localparam [ 3:0] SDRAM_STATE_WAIT_RESET        = 4'd0;
localparam [ 3:0] SDRAM_STATE_INIT_PRECHARGE    = 4'd1;
localparam [ 3:0] SDRAM_STATE_SET_MODE_REG      = 4'd2;
localparam [ 3:0] SDRAM_STATE_IDLE              = 4'd3;
localparam [ 3:0] SDRAM_STATE_BANK_ACT          = 4'd4;
localparam [ 3:0] SDRAM_STATE_READ              = 4'd5;
localparam [ 3:0] SDRAM_STATE_WRITE             = 4'd6;
localparam [ 3:0] SDRAM_STATE_PRECHARGE         = 4'd7;
localparam [ 3:0] SDRAM_STATE_WAIT_RC           = 4'd8;
localparam [ 3:0] SDRAM_STATE_REFRESH_A         = 4'd9;
localparam [ 3:0] SDRAM_STATE_REFRESH_B         = 4'd10;

localparam [ 2:0] CAS_LATENCY = 2;
localparam [ 1:0] RCD_LATENCY = 1;
localparam [ 3:0] RC_LATENCY  = 6;

reg [ 4:0] ramResetDelayCounter;
reg [ 2:0] command;
reg [ 3:0] state;
reg [ 3:0] nextState;
reg [ 3:0] initialRefreshCycleCount;
reg [ 3:0] nopCounter;
reg [ 3:0] rcCounter;

assign { sdram_dqmh, sdram_dqml } = 2'b00;
assign sdram_ba = 2'b00;

reg [11:0] address;

wire ramResetDelayDone  = ~|ramResetDelayCounter;

wire chipSelect = command != SDRAM_CMD_NOP;
assign sdram_cs_n   = ~chipSelect;
assign sdram_ras_n  = ~chipSelect | command[2];
assign sdram_cas_n  = ~chipSelect | command[1];
assign sdram_we_n   = ~chipSelect | command[0];

assign sdram_addr[10:0] = address[10:0];
assign sdram_addr[11]  = 1'b0;

//assign sdram_clk = dramclk;
ODDR2 ODDR_clk(
    .Q(sdram_clk),
    .C0(dramclk),
    .C1(~dramclk),
    .CE(1'b1),
    .D0(1'b1),
    .D1(1'b0),
    .R(1'b0),
    .S(1'b0)
);

assign sdram_cke = ~rst;

// Startup delay logic
always @(negedge dramclk) begin
    if (rst)
        ramResetDelayCounter <= 5'd31;
    else if (!ramResetDelayDone)
        ramResetDelayCounter <= ramResetDelayCounter - 5'd1;
end

reg [ 8:0] ramRefreshCounter;
reg refresh_rq;

always @(negedge dramclk) begin
    if (rst)
        ramRefreshCounter <= 0;
    else begin
        if (~|ramRefreshCounter)
            refresh_rq <= 1'b1;

        if (state == SDRAM_STATE_REFRESH_B)
            refresh_rq <= 1'b0;

        ramRefreshCounter <= ramRefreshCounter + 9'd1;
    end
end

wire [10:0] rowAddress      = in_address[18:8];
wire [10:0] columnAddress   = { 1'b1, 2'd0, in_address[ 7:0] }; // auto precharge

wire write_rq;
rise_edge_latch write_req_latch(dramclk, write_req, write_rq, state == SDRAM_STATE_WRITE);

wire read_rq;
rise_edge_latch read_req_latch(dramclk, read_req, read_rq, state == SDRAM_STATE_READ);

always @(negedge dramclk) begin
    if (rst)
        state <= SDRAM_STATE_WAIT_RESET;
    else if (~|nopCounter)
        state <= nextState;
end

assign ready = state == SDRAM_STATE_IDLE;

always @(*) begin
    case (state)
        SDRAM_STATE_WAIT_RESET: begin
            if (ramResetDelayDone)
                nextState <= SDRAM_STATE_INIT_PRECHARGE;
            else
                nextState <= SDRAM_STATE_WAIT_RESET;
        end
        SDRAM_STATE_INIT_PRECHARGE: begin
            nextState <= SDRAM_STATE_SET_MODE_REG;
        end
        SDRAM_STATE_SET_MODE_REG: begin
            nextState <= SDRAM_STATE_IDLE;
        end
        SDRAM_STATE_IDLE: begin
            if (read_rq ^ write_rq) // <- We got a read or write request (both signals being set is considered illegal)
                nextState       <= SDRAM_STATE_BANK_ACT;
            else if (refresh_rq & ~refresh_inhibit) // <- Refresh if needed
                nextState       <= SDRAM_STATE_REFRESH_A;
            else // <- Remain idle
                nextState   <= SDRAM_STATE_IDLE;
        end
        SDRAM_STATE_BANK_ACT: begin
            nextState <= read_rq ? SDRAM_STATE_READ : SDRAM_STATE_WRITE;
        end
        SDRAM_STATE_READ: begin
            nextState <= SDRAM_STATE_WAIT_RC;
        end
        SDRAM_STATE_WRITE: begin
            nextState <= SDRAM_STATE_WAIT_RC;
        end
        SDRAM_STATE_PRECHARGE: begin
            nextState <= SDRAM_STATE_WAIT_RC;
        end
        SDRAM_STATE_WAIT_RC: begin
            if (~|rcCounter)
                nextState <= SDRAM_STATE_IDLE;
            else
                nextState <= SDRAM_STATE_WAIT_RC;
        end
        SDRAM_STATE_REFRESH_A: begin
            nextState <= SDRAM_STATE_REFRESH_B;
        end
        SDRAM_STATE_REFRESH_B: begin
            nextState <= SDRAM_STATE_WAIT_RC;
        end
        default: nextState <= SDRAM_STATE_IDLE;
    endcase
end

always @(*) begin
    if (|nopCounter) begin
        command <= SDRAM_CMD_NOP;
    end else begin
        case (state)
            SDRAM_STATE_INIT_PRECHARGE: command <= SDRAM_CMD_PRECHARGE_ALL;
            SDRAM_STATE_SET_MODE_REG:   command <= SDRAM_CMD_MODE_REG_SET;
            SDRAM_STATE_BANK_ACT:       command <= SDRAM_CMD_BANK_ACTIVE;
            SDRAM_STATE_READ:           command <= SDRAM_CMD_READ;
            SDRAM_STATE_WRITE:          command <= SDRAM_CMD_WRITE;
            SDRAM_STATE_PRECHARGE:      command <= SDRAM_CMD_PRECHARGE_ALL;
            SDRAM_STATE_REFRESH_A:      command <= SDRAM_CMD_PRECHARGE_ALL;
            SDRAM_STATE_REFRESH_B:      command <= SDRAM_CMD_REFRESH;
            default:                    command <= SDRAM_CMD_NOP;
        endcase
    end
end

always @(*) begin
    case (state)
        SDRAM_STATE_INIT_PRECHARGE: address <= 11'b10000000000;
        SDRAM_STATE_SET_MODE_REG:   address <= SDRAM_SETTINGS;
        SDRAM_STATE_BANK_ACT:       address <= rowAddress;
        SDRAM_STATE_READ:           address <= columnAddress;
        SDRAM_STATE_WRITE:          address <= columnAddress;
        SDRAM_STATE_PRECHARGE:      address <= 11'b10000000000;
        SDRAM_STATE_REFRESH_A:      address <= 11'b10000000000;
        SDRAM_STATE_REFRESH_B:      address <= 11'b10000000000;
        default:                    address <= 11'b00000000000;
    endcase
end

always @(negedge dramclk) begin
    if (|rcCounter)  rcCounter  <= rcCounter - 4'd1;
    if (|nopCounter) nopCounter <= nopCounter - 4'd1;

    if (rst) begin
        rcCounter <= 4'd0;
        nopCounter <= 4'd5;
    end

    if (~|rcCounter) begin
        if (state != SDRAM_STATE_IDLE && state != SDRAM_STATE_WAIT_RC) begin
            rcCounter   <= RC_LATENCY;
        end
    end

    if (~|nopCounter) begin
        case (state)
            SDRAM_STATE_INIT_PRECHARGE: nopCounter <= 3'd6;
            SDRAM_STATE_SET_MODE_REG:   nopCounter <= 3'd6;
            SDRAM_STATE_BANK_ACT:       nopCounter <= RCD_LATENCY;
            SDRAM_STATE_REFRESH_A:      nopCounter <= RCD_LATENCY;
            SDRAM_STATE_REFRESH_B:      nopCounter <= RCD_LATENCY;
        endcase
    end
end

reg write_out_enable;

always @(negedge dramclk) begin
    write_out_enable <= state == SDRAM_STATE_WRITE;
end

localparam trl = 5;  // total read latency
reg [trl-1:0] dataValidpipe;
always @(negedge dramclk) begin
    dataValidpipe <= {dataValidpipe[trl-2:0], state == SDRAM_STATE_READ};
end

wire data_valid = dataValidpipe[trl-2] & ~dataValidpipe[trl-3];
wire rd_valid = dataValidpipe[trl-1] & ~dataValidpipe[trl-2];

//reg [15:0] out_data_buf;
always @(posedge dramclk) begin
    if (data_valid) begin
        out_data <= sdram_dq[15:8];
    end
end

always @(posedge dramclk) begin
    if (rst | ~read_req) begin
        out_valid <= 1'b0;
    end
    else if (rd_valid) begin
        out_valid <= 1'b1;
    end
end

reg [7:0] WrData1;
always @(posedge dramclk) WrData1 <= in_data;

reg [7:0] WrData2;
always @(negedge dramclk) WrData2 <= WrData1;

assign sdram_dq = write_out_enable ? {WrData2,8'd0} : 16'hzzzz;

endmodule
