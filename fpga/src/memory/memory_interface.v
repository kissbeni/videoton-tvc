`timescale 1ns / 1ps

module memory_interface(
    input  wire        sysclk,
    input  wire        dramclk,
    input  wire        rst,
    input  wire [15:0] addr,
    input  wire [ 7:0] data_in,
    output reg  [ 7:0] data_out,
    input  wire        wr,
    input  wire        rd,
    input  wire        memrq,
    output reg         out_data_valid,
    output reg  [ 7:0] pixDataIn,
    output reg         pixDataWr,
    input  wire [13:0] pixDataAddr,
    input  wire        pixDataWrReq,
    input  wire        dramReady,
    output reg         dramWriteRequest,
    output reg         dramReadRequest,
    input  wire        dramReadValid,
    output wire [15:0] dramAddress,
    output wire [ 7:0] dramWriteData,
    input  wire [ 7:0] dramReadData
);

localparam [15:0] ROM_SIZE = 2048;
wire [10:0] romAddr = addr[10:0];

wire memAnyReadSelect = memrq & rd;

//------------------- DEBUG RAM

reg  [ 7:0] romData [(ROM_SIZE-1):0];

initial begin
    $readmemh("X:/videoton-tvc/roms/dbgrom.txt", romData, 0, ROM_SIZE-1);
end

always @(posedge sysclk) begin
    if (wr & memrq & (addr < ROM_SIZE))
        romData[romAddr] <= data_in;
end

//------------------- DRAM
reg         dramReadValidBuf;
reg  [ 7:0] dramReadDataBuf;
wire        videoRamSelect;
reg         videoRamSelectBuf;
reg         dramReadyBuf;

wire dramAddrSelect = addr >= ROM_SIZE;

wire dramReadSelect = memrq & dramAddrSelect & rd;
wire dramWriteSelect = memrq & dramAddrSelect & wr;

wire dramAnySelect = memrq & dramAddrSelect & (rd | wr);

localparam [1:0] MEMIO_STATE_IDLE = 2'd0;
localparam [1:0] MEMIO_STATE_VIDEORD = 2'd1;
localparam [1:0] MEMIO_STATE_SYSRAMRW = 2'd2;
localparam [1:0] MEMIO_STATE_WAITIDLE = 2'd3;

reg [1:0] state;
reg [1:0] nextState;

wire dramAnySelectTrigger;
wire dramAnySelectAck;
rise_edge_latch dram_ram_select_latch(sysclk, dramAnySelect, dramAnySelectTrigger, dramAnySelectAck);

wire videoRamSelectTrigger;
wire videoRamSelectAck;
rise_edge_latch dram_vram_select_latch(sysclk, pixDataWrReq, videoRamSelectTrigger, videoRamSelectAck);

wire memoryDeselectTrigger;
wire memoryDeselectAck;
fall_edge_latch memrq_deselect_latch(sysclk, memrq, memoryDeselectTrigger, memoryDeselectAck);

always @(posedge sysclk) begin
    if (rst)
        state <= MEMIO_STATE_WAITIDLE;
    else
        state <= nextState;
end

assign videoRamSelectAck = (state == MEMIO_STATE_VIDEORD);
assign videoRamSelect = (state == MEMIO_STATE_VIDEORD) | (nextState == MEMIO_STATE_VIDEORD);
assign dramAddress = videoRamSelect ? {2'b01, pixDataAddr} : addr;
assign dramWriteData = data_in;
assign memoryDeselectAck = (state == MEMIO_STATE_IDLE) & dramAnySelectTrigger;
assign dramAnySelectAck = (state == MEMIO_STATE_SYSRAMRW);

always @(posedge sysclk) begin
    if ((nextState == MEMIO_STATE_SYSRAMRW) | (nextState == MEMIO_STATE_VIDEORD))
        dramReadRequest <= (dramReadSelect & (nextState == MEMIO_STATE_SYSRAMRW)) | (nextState == MEMIO_STATE_VIDEORD);
    else if (dramReady & (state == MEMIO_STATE_WAITIDLE))
        dramReadRequest <= 1'b0;
end

always @(posedge sysclk) begin
    if (nextState == MEMIO_STATE_SYSRAMRW)
        dramWriteRequest <= dramWriteSelect;
    else if (dramReady & (state == MEMIO_STATE_WAITIDLE))
        dramWriteRequest <= 1'b0;
end

always @(*) begin
    case (state)
        MEMIO_STATE_IDLE: begin
            if (dramAnySelectTrigger)
                nextState <= MEMIO_STATE_SYSRAMRW;
            else if (videoRamSelectTrigger & memoryDeselectTrigger)
                nextState <= MEMIO_STATE_VIDEORD;
            else
                nextState <= MEMIO_STATE_IDLE;
        end
        MEMIO_STATE_VIDEORD: begin
            if (~pixDataWrReq | dramReadValid)
                nextState <= MEMIO_STATE_WAITIDLE;
            else
                nextState <= MEMIO_STATE_VIDEORD;
        end
        MEMIO_STATE_SYSRAMRW: begin
            if (~dramReadSelect | dramReadValid)
                nextState <= MEMIO_STATE_WAITIDLE;
            else
                nextState <= MEMIO_STATE_SYSRAMRW;
        end
        MEMIO_STATE_WAITIDLE: begin
            if (dramReady)
                nextState <= MEMIO_STATE_IDLE;
            else
                nextState <= MEMIO_STATE_WAITIDLE;
        end
    endcase
end

reg dramReadyBuf1;
always @(posedge dramclk) begin
    dramReadyBuf1 <= dramReady;
    dramReadyBuf <= dramReadyBuf1;
end

always @(posedge sysclk) begin
    dramReadDataBuf <= dramReadData;
    dramReadValidBuf <= dramReadValid;
    videoRamSelectBuf <= videoRamSelect;
end

always @(posedge sysclk) begin
    if (rst | pixDataWr) pixDataWr <= 1'b0;
    if (dramReadValidBuf & videoRamSelectBuf) begin
        pixDataIn <= dramReadDataBuf;
        pixDataWr <= 1'b1;
    end
end

// Read handler
always @(posedge sysclk) begin
    if (rst) begin
        data_out <= 8'd0;
        out_data_valid <= 1'b0;
    end
    else begin
        if (memAnyReadSelect) begin
            if (dramReadValidBuf & ~videoRamSelectBuf) begin
                data_out <= dramReadDataBuf;
                out_data_valid <= 1'b1;
            end
            if (addr < ROM_SIZE) begin
                data_out <= romData[romAddr];
                out_data_valid <= 1'b1;
            end
        end else
            out_data_valid <= 1'b0;
    end
end

endmodule
