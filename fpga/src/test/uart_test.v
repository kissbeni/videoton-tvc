`timescale 1ns / 1ps

module uart_test;

// Inputs
reg         wr_start;
reg   [7:0] data;
wire        clk;
reg         clk_enable;
reg         rst;

// Outputs
wire        uart_busy;
wire        uart_tx;
wire [ 7:0] rx_data;
wire        rx_valid;

// Instantiate the Unit Under Test (UUT)
uart_tx uut (
    .uart_busy(uart_busy),
    .uart_tx(uart_tx),
    .wr_start(wr_start),
    .data(data),
    .clk(clk),
    .rst(rst)
);

uart_rx uut2 (
    .clk(clk),
    .rst(rst),
    .uart_rx(uart_tx),
    .data(rx_data),
    .valid(rx_valid)
);

wire rx_fifo_empty;
wire rx_fifo_full;

wire [7:0] rx_fifo_out;

reg rx_fifo_take;

uart_rx_fifo uut3(
    .sysclk(clk),
    .rst(rst),
    .take(rx_fifo_take),
    .put(rx_valid),
    .empty(rx_fifo_empty),
    .full(rx_fifo_full),
    .data_out(rx_fifo_out),
    .data_in(rx_data)
);

clock_gen #(.FREQ(50_000)) ckgen(clk_enable, clk);

reg [3:0] clkdivctr = 0;
always @(posedge clk) begin
    clkdivctr <= clkdivctr + 4'd1;
end

wire cpuclk = clkdivctr[3];
wire cpuclkPosedge = clkdivctr == 3'd111;

reg [7:0] takenData;
reg [1:0] takeDelayCntr = 0;
reg acceptData;

always @(posedge clk) begin
    if (rx_fifo_take) rx_fifo_take <= 1'b0;
    if (cpuclkPosedge) begin
        takeDelayCntr <= takeDelayCntr + 1'd1;
        if (~|takeDelayCntr & acceptData & ~rx_fifo_empty) begin
            rx_fifo_take <= 1'b1;
            takenData <= rx_fifo_out;
            $display("taken: %02x", rx_fifo_out);
        end
    end
end

initial begin
    // Initialize Inputs
    wr_start <= 0;
    data <= 0;
    rst <= 1;
    rx_fifo_take <= 1'b0;
    clk_enable <= 1;
    acceptData <= 0;

    #400;

    rst <= 0;

    @(negedge uart_busy);
    #100;
    @(negedge clk);
    data <= 8'h53;
    wr_start <= 1;
    @(negedge clk);
    wr_start <= 0;
    @(negedge uart_busy);
    
    @(negedge clk);
    data <= 8'h42;
    wr_start <= 1;
    @(negedge clk);
    wr_start <= 0;
    @(negedge uart_busy);
    
    acceptData <= 1;
    
    @(negedge clk);
    data <= 8'hcd;
    wr_start <= 1;
    @(negedge clk);
    wr_start <= 0;
    @(negedge uart_busy);
    
    acceptData <= 0;
    
    @(negedge clk);
    data <= 8'h12;
    wr_start <= 1;
    @(negedge clk);
    wr_start <= 0;
    @(negedge uart_busy);
    
    acceptData <= 1;
    
    @(posedge cpuclk);
    @(posedge cpuclk);
    @(posedge cpuclk);
    @(posedge cpuclk);
    @(posedge cpuclk);
    
    $finish;
    //wr_start <= 1;
end

endmodule
