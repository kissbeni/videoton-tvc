`timescale 1ns / 1ps

module test_cpu_mem_interface;

// Inputs
wire cpuclk;
wire dramclk;
wire sysclk;
reg  [15:0] addr;
reg  [ 7:0] data_in;
reg         wr;
reg         rd;
reg         memrq;
reg         clk_enable;
reg         rst;
wire [ 7:0] pixDataIn;
wire        pixDataWr;

// Outputs
wire [ 7:0] data_out;
wire        out_data_valid;
wire        vga_hsync;
wire        vga_vsync;
wire [ 4:0] vga_r;
wire [ 5:0] vga_g;
wire [ 4:0] vga_b;
wire [13:0] pixDataAddr;
wire        pixDataWrReq;
wire        cpu_wait;

// Bidirs
wire [15:0] sdram_data;

wire [12:0] sdram_addr;
wire [ 1:0] sdram_ba;
wire        sdram_cke;
wire        sdram_clk;
wire        sdram_cs_n;
wire        sdram_we_n;
wire        sdram_cas_n;
wire        sdram_ras_n;

clock_gen #(.FREQ(100_000)) u1(clk_enable, dramclk);

wire [1:0] Dqm;
sdr SDRAMIC(
    sdram_data,
    sdram_addr[11:0],
    sdram_ba,
    sdram_clk,
    sdram_cke,
    sdram_cs_n,
    sdram_ras_n,
    sdram_cas_n,
    sdram_we_n,
    Dqm
);

// Instantiate the Unit Under Test (UUT)
vga_controller vga(
    .clk(sysclk),
    .rst(rst),
    .vga_hsync(vga_hsync),
    .vga_vsync(vga_vsync),
    .vga_r(vga_r),
    .vga_g(vga_g),
    .vga_b(vga_b),
    .pixDataIn(pixDataIn),
    .pixDataWr(pixDataWr),
    .pixDataAddr(pixDataAddr),
    .pixDataWrReq(pixDataWrReq)
);

// Instantiate the Unit Under Test (UUT)
memory_interface uut (
    .sysclk(sysclk),
    .dramclk(dramclk),
    .rst(rst),
    .addr(addr),
    .data_in(data_in),
    .data_out(data_out),
    .wr(wr),
    .rd(rd),
    .memrq(memrq),
    .out_data_valid(out_data_valid),
    .pixDataIn(pixDataIn),
    .pixDataWr(pixDataWr),
    .pixDataAddr(pixDataAddr),
    .pixDataWrReq(pixDataWrReq),
    .cpu_wait(cpu_wait),
    .sdram_addr(sdram_addr),
    .sdram_bank(sdram_ba),
    .sdram_cke(sdram_cke),
    .sdram_clk(sdram_clk),
    .sdram_cs_n(sdram_cs_n),
    .sdram_dqm(Dqm),
    .sdram_we_n(sdram_we_n),
    .sdram_cas_n(sdram_cas_n),
    .sdram_ras_n(sdram_ras_n),
    .sdram_data(sdram_data)
);

reg [4:0] clkdivctr = 0;
always @(posedge dramclk) begin
    clkdivctr <= clkdivctr + 5'd1;
end

assign cpuclk = clkdivctr[3];
assign sysclk = clkdivctr[0];

initial begin
    // Initialize Inputs
    addr <= 0;
    data_in <= 0;
    wr <= 0;
    rd <= 0;
    memrq <= 0;
    rst <= 1;
    clk_enable <= 1;

    #100;
    rst <= 0;

    @(negedge cpuclk);
    @(negedge cpuclk);
    @(negedge cpuclk);
    @(negedge cpuclk);
    @(negedge cpuclk); #1;

    memrq <= 1'b1;
    addr <= 16'hff00;
    data_in <= 8'hAF;

    @(negedge cpuclk); #1;
    wr <= 1'b1;
    @(negedge cpuclk); #1;

    memrq <= 1'b0;
    wr <= 1'b0;
    addr <= 16'h0000;

    @(negedge cpuclk);  #1;

    memrq <= 1'b1;
    rd <= 1'b1;
    addr <= 16'hff00;

    @(negedge cpuclk); #1;
    @(negedge cpuclk); #1;

    memrq <= 1'b0;
    rd <= 1'b0;
    addr <= 16'h0000;

    @(negedge cpuclk); #1;
end

endmodule

