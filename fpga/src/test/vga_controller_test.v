`timescale 1ns / 1ps

module vga_controller_test;

// Inputs
wire clk;
reg clk_enable, rst;
reg [15:0] widePixDataIn;
reg pixDataWr;

// Outputs
wire vga_hsync;
wire vga_vsync;
wire [4:0] vga_r;
wire [5:0] vga_g;
wire [4:0] vga_b;
wire [12:0] widePixDataAddr;
wire pixDataWrReq;

// Instantiate the Unit Under Test (UUT)
vga_controller uut (
    .clk(clk),
    .vga_hsync(vga_hsync),
    .vga_vsync(vga_vsync),
    .vga_r(vga_r),
    .vga_g(vga_g),
    .vga_b(vga_b),
    .widePixDataIn(widePixDataIn),
    .pixDataWr(pixDataWr),
    .widePixDataAddr(widePixDataAddr),
    .pixDataWrReq(pixDataWrReq)
);

clock_gen #(.FREQ(50_000)) ckgen(clk_enable, clk);

always @(posedge pixDataWrReq) begin
    widePixDataIn <= {3'd0, widePixDataAddr};
    pixDataWr = 1'b1;
end

always @(negedge pixDataWrReq) begin
    pixDataWr = 1'b0;
end

initial begin
    rst <= 1;
    clk_enable <= 1;

    #100;
    rst <= 0;
end

endmodule

