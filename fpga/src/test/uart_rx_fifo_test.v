`timescale 1ns / 1ps

module uart_rx_fifo_test;

// Inputs
wire sysclk;
reg clk_enable;
reg rst;
reg take;
reg put;
reg [7:0] data_in;

// Outputs
wire empty;
wire full;
wire [7:0] data_out;

// Instantiate the Unit Under Test (UUT)
uart_rx_fifo uut (
    .sysclk(sysclk), 
    .rst(rst), 
    .take(take), 
    .put(put), 
    .empty(empty), 
    .full(full), 
    .data_out(data_out), 
    .data_in(data_in)
);

clock_gen #(.FREQ(50_000)) u1(clk_enable, sysclk);

integer i;

initial begin
    // Initialize Inputs
    clk_enable <= 1'b1;
    rst <= 1'b1;
    take <= 0;
    put <= 0;
    data_in <= 0;

    // Wait 100 ns for global reset to finish
    #100;
    
    rst <= 1'b0;
    
    for (i = 0; i < 16; i = i + 1) begin    
        @(posedge sysclk);
        data_in <= i;
        put <= 1'b1;
        @(posedge sysclk);
        put <= 1'b0;
        
        @(posedge sysclk);
        $display("%02x", data_out);
        take <= 1'b1;
        @(posedge sysclk);
        take <= 1'b0;
    end
    
    $display("-------------------");
    
    for (i = 0; i < 14; i = i + 1) begin    
        @(posedge sysclk);
        data_in <= i;
        put <= 1'b1;
        @(posedge sysclk);
        put <= 1'b0;
        
        @(posedge sysclk);
        data_in <= i;
        put <= 1'b1;
        @(posedge sysclk);
        put <= 1'b0;
        
        @(posedge sysclk);
        $display("%02x", data_out);
        take <= 1'b1;
        @(posedge sysclk);
        take <= 1'b0;
    end
    
    for (i = 0; i < 14; i = i + 1) begin
        @(posedge sysclk);
        $display("%02x", data_out);
        take <= 1'b1;
        @(posedge sysclk);
        take <= 1'b0;
    end

    // Add stimulus here

end

endmodule

