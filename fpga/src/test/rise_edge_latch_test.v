`timescale 1ns / 1ps

module rise_edge_latch_test;

reg clk_enable;

// Inputs
wire clk;
reg in;
reg ack;

// Outputs
wire trigger;

// Instantiate the Unit Under Test (UUT)
rise_edge_latch uut (
    .clk(clk), 
    .in(in), 
    .trigger(trigger), 
    .ack(ack)
);

clock_gen #(.FREQ(50_000)) u1(clk_enable, clk);

initial begin
    in <= 0;
    ack <= 0;
    clk_enable <= 1;

    #100;
    
    #1; @(posedge clk);
    #1; @(posedge clk);
    #1; @(posedge clk);
    
    in <= 1'b1;
    @(posedge trigger);
    #1; @(posedge clk);
    #1; @(posedge clk);
    #1; @(posedge clk);
    ack <= 1'b1;
    @(negedge trigger);
    #1; @(posedge clk);
    #1; @(posedge clk);
    ack <= 1'b0;
    #1; @(posedge clk);
    #1; @(posedge clk);
    in <= 1'b0;

    // Add stimulus here

end

endmodule
