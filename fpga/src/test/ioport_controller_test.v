`timescale 1ns / 1ps

module ioport_controller_test;

// Inputs
wire clk;
wire sysclk;
wire cpuclk;
reg clk_enable;
reg rst;
reg [7:0] addr;
reg [7:0] data_in;
reg rd;
reg wr;
reg iorq;
wire cpuclkRise;
reg bt;
reg uart_rx;

// Outputs
wire [7:0] data_out;
wire [2:0] ld;
wire uart_tx;
wire [7:0] ssd_seg;
wire [3:0] ssd_dig;
wire sound;

// Instantiate the Unit Under Test (UUT)
ioport_controller uut (
    .sysclk(sysclk), 
    .cpuclk(cpuclk), 
    .rst(rst), 
    .addr(addr), 
    .data_in(data_in), 
    .data_out(data_out), 
    .rd(rd), 
    .wr(wr), 
    .iorq(iorq), 
    .cpuclkRise(cpuclkRise), 
    .bt(bt), 
    .ld(ld), 
    .uart_rx(uart_rx), 
    .uart_tx(uart_tx), 
    .ssd_seg(ssd_seg), 
    .ssd_dig(ssd_dig), 
    .sound(sound)
);

clock_gen #(.FREQ(100_000)) u1(clk_enable, clk);

reg [4:0] clkdivctr = 0;
always @(posedge clk) begin
    clkdivctr <= clkdivctr + 5'd1;
end

assign cpuclk = clkdivctr[3];
assign sysclk = clkdivctr[0];

reg [1:0] cpuclkRiseBuf;

always @(posedge sysclk) begin
    cpuclkRiseBuf <= {cpuclkRiseBuf[0], cpuclk};
end

assign cpuclkRise = cpuclkRiseBuf == 2'b01;

initial begin
    // Initialize Inputs
    addr = 0;
    data_in = 0;
    rd = 0;
    wr = 0;
    iorq = 0;
    bt = 0;
    uart_rx = 0;
    
    rst <= 1;
    clk_enable <= 1;

    #100;
    rst <= 0;
    
    #10; @(negedge cpuclk);
    #10; @(negedge cpuclk);
    #10; @(negedge cpuclk);
    #10; @(negedge cpuclk);
    #10; @(negedge cpuclk);

    addr <= 8'h04;
    data_in <= 8'hAF;
    
    #10; @(posedge cpuclk);
    iorq <= 1'b1;
    wr <= 1'b1;
    #10; @(posedge cpuclk);
    #10; @(negedge cpuclk);

    iorq <= 1'b0;
    wr <= 1'b0;
    
    #10; @(posedge cpuclk);
    addr <= 8'h00;
    data_in <= 8'h00;

    #10; @(negedge cpuclk);
    /////////////////////////////////////////////////////
    #10; @(negedge cpuclk);

    addr <= 8'h05;
    data_in <= 8'h1f;
    
    #10; @(posedge cpuclk);
    iorq <= 1'b1;
    wr <= 1'b1;
    #10; @(posedge cpuclk);
    #10; @(negedge cpuclk);

    iorq <= 1'b0;
    wr <= 1'b0;
    
    #10; @(posedge cpuclk);
    addr <= 8'h00;
    data_in <= 8'h00;

    #10; @(negedge cpuclk);
end

endmodule

