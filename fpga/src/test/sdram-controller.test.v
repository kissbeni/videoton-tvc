`timescale 1ns/1ps

module SDRAMTest;

wire         clk;
reg          clk_enable;
reg          rst;
reg   [19:0] address;
reg   [ 7:0] in_data;
reg   [ 1:0] bank;
reg          write_req;
reg          read_req;
reg          refresh_inhibit;

wire  [11:0] sdram_addr;
wire  [ 1:0] sdram_ba;
wire         sdram_cke;
wire         sdram_clk;
wire         sdram_cs_n;
wire         sdram_dqmh;
wire         sdram_dqml;
wire         sdram_we_n;
wire         sdram_cas_n;
wire         sdram_ras_n;
wire [15:0]  sdram_dq;

wire         ready, out_valid;
wire [ 7:0]  out_data;

clock_gen #(.FREQ(100_000)) u1(clk_enable, clk);

sdram_controller _sdram_controller(
    sdram_addr,
    sdram_ba,
    sdram_cke,
    sdram_clk,
    sdram_cs_n,
    sdram_dqmh,
    sdram_dqml,
    sdram_we_n,
    sdram_cas_n,
    sdram_ras_n,
    sdram_dq,

    ready,
    write_req,
    read_req,
    out_valid,

    clk,
    rst,
    refresh_inhibit,

    address,
    in_data,
    out_data
);

wire [1:0] Dqm = {sdram_dqmh, sdram_dqml};
sdr SDRAMIC(
    sdram_dq,
    sdram_addr,
    sdram_ba,
    sdram_clk,
    sdram_cke,
    sdram_cs_n,
    sdram_ras_n,
    sdram_cas_n,
    sdram_we_n,
    Dqm
);

reg [3:0] clkdivctr = 0;
always @(posedge clk) begin
    clkdivctr <= clkdivctr + 4'd1;
end

wire cpuclk = clkdivctr[3];

initial begin
    //$dumpfile("my_dumpfile.vcd");
    //$dumpvars(0, _sdram_controller);

    rst <= 1;
    clk_enable <= 1;
    refresh_inhibit <= 0;

    read_req <= 1'b0;
    write_req <= 1'b0;
    #100;
    rst <= 0;

    @(posedge ready);
    @(posedge cpuclk);
    $display("Writing...");

    // Write
    address <= 20'h7adb3;
    bank <= 2'd1;
    in_data <= 8'hbe;
    write_req <= 1'b1;
    @(posedge ready);
    write_req <= 1'b0;
    @(posedge cpuclk);
    in_data <= 8'h0;

    $display("Reading...");

    // Read
    address <= 20'h7adb3;
    bank <= 2'd1;
    read_req <= 1'b1;
    @(posedge ready);
    read_req <= 1'b0;
    @(posedge cpuclk);

    $display("Reading...");

    // Read
    address <= 20'h7adb3;
    bank <= 2'd1;
    read_req <= 1'b1;
    @(posedge ready);
    read_req <= 1'b0;
    @(posedge cpuclk);

    #30;

    $display("Finished");

    #2;
    //$finish;
end

endmodule
