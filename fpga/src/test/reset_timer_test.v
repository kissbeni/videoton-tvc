`timescale 1ns / 1ps

module reset_timer_test;

// Inputs
wire sysclk;
wire dramclk;
wire cpuclk;
reg clk_enable;
reg dirty_rst;
wire dram_ready;

// Outputs
wire sysrst;
wire cpurst_n;
wire dramrst;

// Instantiate the Unit Under Test (UUT)
reset_timer uut (
    .sysclk(sysclk), 
    .dramclk(dramclk), 
    .cpuclk(cpuclk), 
    .dirty_rst(dirty_rst), 
    .sysrst(sysrst), 
    .cpurst_n(cpurst_n), 
    .dramrst(dramrst), 
    .dram_ready(dram_ready)
);

clock_gen #(.FREQ(100_000)) u1(clk_enable, dramclk);

reg [4:0] clkdivctr = 0;
always @(posedge dramclk) begin
    clkdivctr <= clkdivctr + 5'd1;
end

assign cpuclk = clkdivctr[3];
assign sysclk = clkdivctr[0];

reg [3:0] dramResetSim;

assign dram_ready = dramResetSim[3];

always @(posedge dramclk) begin
    if (dramrst) begin
        dramResetSim <= 4'd0;
    end else if (~dram_ready) begin
        dramResetSim <= dramResetSim + 4'd1;
    end
end

initial begin
    // Initialize Inputs
    #20;
    clk_enable <= 1;
    dirty_rst <= 0;

    #1400;
    
    dirty_rst <= 1;
    #5;
    dirty_rst <= 0;
    #1;
    dirty_rst <= 1;
    #2
    dirty_rst <= 0;
    #1;
    dirty_rst <= 1;
    #100;
    dirty_rst <= 0;
    

    // Add stimulus here

end
      
endmodule

