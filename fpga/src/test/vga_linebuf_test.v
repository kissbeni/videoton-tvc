`timescale 1ns / 1ps

module vga_linebuf_test;

// Inputs
wire sysclk;
reg clk_enable;
reg [6:0] writeAddr;
reg [7:0] writeData;
reg colorSel;
reg store;
reg [6:0] readAddr;
reg lineRegisterSelect;

// Outputs
wire [7:0] readData;
wire [7:0] readColorData;

// Instantiate the Unit Under Test (UUT)
video_line_buffer uut (
    .sysclk(sysclk), 
    .writeAddr(writeAddr), 
    .writeData(writeData), 
    .colorSel(colorSel), 
    .store(store), 
    .readAddr(readAddr), 
    .readData(readData), 
    .readColorData(readColorData), 
    .lineRegisterSelect(lineRegisterSelect)
);

clock_gen #(.FREQ(50_000)) ckgen(clk_enable, sysclk);

initial begin
    // Initialize Inputs
    clk_enable <= 1'b1;
    writeAddr <= 0;
    writeData <= 0;
    colorSel <= 0;
    store <= 0;
    readAddr <= 0;
    lineRegisterSelect <= 0;

    // Wait 100 ns for global reset to finish
    #100;
    
    @(posedge sysclk);
    writeAddr <= 8'h15;
    writeData <= 8'had;
    colorSel <= 1'b0;
    store <= 1'b1;
    @(posedge sysclk);
    store <= 1'b0;
    
    @(posedge sysclk);
    @(posedge sysclk);
    @(posedge sysclk);
    writeAddr <= 8'h15;
    writeData <= 8'had;
    colorSel <= 1'b1;
    store <= 1'b1;
    @(posedge sysclk);
    store <= 1'b0;
    colorSel <= 1'b0;
    
    @(posedge sysclk);
    @(posedge sysclk);
    @(posedge sysclk);
    writeAddr <= 8'h15;
    writeData <= 8'had;
    colorSel <= 1'b0;
    store <= 1'b1;
    @(posedge sysclk);
    store <= 1'b0;

    // Add stimulus here

end

endmodule

