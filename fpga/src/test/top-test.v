`timescale 1ns/1ps

module top_test;

// Inputs
wire        clk;
reg  [ 3:0] bt;

// Outputs
wire [ 7:0] ssd_seg;
wire [ 3:0] ssd_dig;
wire [ 3:0] ld;
wire        uart_tx;
wire [10:0] sdram_addr;
wire [ 1:0] sdram_bank;
wire        sdram_cke;
wire        sdram_clk;
wire        sdram_cs_n;
wire [ 1:0] sdram_dqm;
wire        sdram_we_n;
wire        sdram_cas_n;
wire        sdram_ras_n;
wire [ 3:0] extdbg;

// Bidirs
wire [15:0] sdram_data;

// Instantiate the Unit Under Test (UUT)
topm uut (
    .clk(clk),
    .bt(bt),
    .ssd_seg(ssd_seg),
    .ssd_dig(ssd_dig),
    .ld(ld),
    .uart_tx(uart_tx),
    .sdram_addr(sdram_addr),
    .sdram_bank(sdram_bank),
    .sdram_cke(sdram_cke),
    .sdram_clk(sdram_clk),
    .sdram_cs_n(sdram_cs_n),
    .sdram_dqm(sdram_dqm),
    .sdram_we_n(sdram_we_n),
    .sdram_cas_n(sdram_cas_n),
    .sdram_ras_n(sdram_ras_n),
    .sdram_data(sdram_data),
    .extdbg(extdbg)
);

sdr SDRAMIC(
    sdram_data,
    sdram_addr,
    sdram_bank,
    sdram_clk,
    sdram_cke,
    sdram_cs_n,
    sdram_ras_n,
    sdram_cas_n,
    sdram_we_n,
    sdram_dqm
);

clock_gen #(.FREQ(50_000)) u1(1'b1, clk);

initial begin
    bt = 4'b0000;
    #20;
    bt = 4'b1111;

    // Wait 100 ns for global reset to finish
    #1000;

    // Add stimulus here
end

endmodule

