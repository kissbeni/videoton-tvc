
// 50MHz / 9600      = 5208
// 50MHz / 38400     = 1302
// 50Mhz / 57600     = 868
// 50Mhz / 115200    = 434
// 50Mhz / 230400    = 217

localparam UART_BITRATE_DIV = 1302;
